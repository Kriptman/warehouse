Training project
================

Learning how to create soap application in Java with Apache CXF

## Involved frameworks:
 * Apache CXF
 * Spring Boot
 * Postgresql
 * Liquibase
 * Lombok
 * JUnit 4 & Mockito
 * Mapstruct
 
## Security
 There are two hardcoded users - "user" & "admin" with password "password".
 
 Request sample: 
```xml
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
     <soapenv:Header>
         <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
             <wsse:UsernameToken>
                 <wsse:Username>admin</wsse:Username>
                 <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">password</wsse:Password>
             </wsse:UsernameToken>
         </wsse:Security>
     </soapenv:Header>
     <soapenv:Body>
         <NewGroup xmlns="http://group.entities.warehouse.andrey.com/">
             <group xmlns="">                 
                 <name>New Group</name>
             </group>
             <parentId xmlns="">0</parentId>
         </NewGroup>
     </soapenv:Body>
 </soapenv:Envelope>
```