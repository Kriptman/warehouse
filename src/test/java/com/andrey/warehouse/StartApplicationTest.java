package com.andrey.warehouse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
public class StartApplicationTest {

    @SuppressWarnings("EmptyMethod")
    @Test
    public void contextLoads() {
    }
}
