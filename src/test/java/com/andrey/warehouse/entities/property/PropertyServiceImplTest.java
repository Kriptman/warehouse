package com.andrey.warehouse.entities.property;

import com.andrey.warehouse.exceptions.InternalException;
import com.andrey.warehouse.exceptions.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class PropertyServiceImplTest {

    @Mock
    private PropertyRepository repository;
    @Mock
    private PropertyMapper mapper;
    private PropertyService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        service = new PropertyServiceImpl(repository, mapper);
    }

    @Test
    public void findPropertyDtoById() {
        Property property = new Property();
        PropertyDto dto = new PropertyDto();

        when(mapper.toPropertyDto(property)).thenReturn(dto);
        when(repository.findById(1L)).thenReturn(Optional.of(property));

        assertEquals(dto, service.findPropertyDtoById(1L));
        verify(repository, times(1)).findById(1L);
        verify(mapper, times(1)).toPropertyDto(property);
    }

    @Test
    public void findPropertyByName() {
        Property property = new Property();

        when(repository.findFirstByName("name")).thenReturn(Optional.of(property));

        assertEquals(property, service.findPropertyByName("name"));
    }

    @Test(expected = NotFoundException.class)
    public void findPropertyByNameNotFound() {
        when(repository.findFirstByName("name")).thenReturn(Optional.empty());

        service.findPropertyByName("name");
    }

    @Test
    public void findPropertyDtoByName() {
        Property property = new Property();
        PropertyDto dto = new PropertyDto();

        when(repository.findFirstByName("name")).thenReturn(Optional.of(property));
        when(mapper.toPropertyDto(property)).thenReturn(dto);

        assertEquals(dto, service.findPropertyDtoByName("name"));
        verify(mapper, times(1)).toPropertyDto(property);
        verify(repository, times(1)).findFirstByName("name");
    }

    @Test
    public void findProperties() {
        PropertyDto propertyDto1 = new PropertyDto();
        propertyDto1.setId(1L);
        propertyDto1.setName("1");
        PropertyDto propertyDto2 = new PropertyDto();
        propertyDto2.setId(2L);
        propertyDto2.setName("2");
        PropertyDto propertyDto3 = new PropertyDto();
        propertyDto3.setId(3L);
        propertyDto3.setName("3");

        Property property1 = new Property();
        property1.setId(1L);
        property1.setName("1");
        Property property2 = new Property();
        property2.setId(2L);
        property2.setName("2");
        Property property3 = new Property();
        property3.setId(3L);
        property3.setName("3");

        Set<Property> properties = Stream.of(property1, property2, property3).collect(Collectors.toSet());
        Set<PropertyDto> propertyDtoSet = Stream.of(propertyDto1, propertyDto2, propertyDto3).collect(Collectors.toSet());

        when(repository.findAll()).thenReturn(properties);
        when(mapper.toPropertyDtoSet(properties)).thenReturn(propertyDtoSet);

        Set<PropertyDto> result = service.findProperties();
        assertEquals(3, result.size());
        verify(repository, times(1)).findAll();
        verify(mapper, times(1)).toPropertyDtoSet(properties);
    }

    @Test
    public void findById() {
        Property property = new Property();

        when(repository.findById(1L)).thenReturn(Optional.of(property));

        assertEquals(property, service.findById(1L));
    }

    @Test(expected = NotFoundException.class)
    public void findByIdNotFound() {
        when(repository.findById(1L)).thenReturn(Optional.empty());

        service.findById(1L);
    }

    @Test
    public void saveProperty() {
        Property property = new Property();
        PropertyDto dto = new PropertyDto();

        when(mapper.toPropertyDto(property)).thenReturn(dto);
        when(mapper.toProperty(dto)).thenReturn(property);
        when(repository.save(property)).thenReturn(property);

        assertEquals(dto, service.saveProperty(dto));
        verify(mapper, times(1)).toPropertyDto(property);
        verify(mapper, times(1)).toProperty(dto);
        verify(repository, times(1)).save(property);
    }

    @Test
    public void updateProperty() {
        Property property = new Property();
        property.setId(1L);
        PropertyDto dto = new PropertyDto();
        dto.setId(1L);

        when(repository.findById(1L)).thenReturn(Optional.of(property));
        when(mapper.toProperty(dto)).thenReturn(property);
        when(repository.save(property)).thenReturn(property);
        when(mapper.toPropertyDto(property)).thenReturn(dto);

        assertEquals(dto, service.updateProperty(dto));
        verify(repository, times(1)).findById(1L);
        verify(mapper, times(1)).toProperty(dto);
        verify(repository, times(1)).save(property);
        verify(mapper, times(1)).toPropertyDto(property);
    }

    @Test
    public void updatePropertyNull() {
        assertNull(service.updateProperty(null));
    }

    @Test(expected = InternalException.class)
    public void updatePropertyInternalException() {
        Property property = new Property();
        property.setId(1L);
        PropertyDto dto = new PropertyDto();
        dto.setId(1L);

        when(repository.findById(1L)).thenReturn(Optional.of(property));
        when(mapper.toProperty(dto)).thenReturn(null);

        service.updateProperty(dto);
    }
}