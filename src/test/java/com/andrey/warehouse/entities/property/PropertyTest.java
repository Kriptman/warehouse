package com.andrey.warehouse.entities.property;

import com.andrey.warehouse.entities.wareProperty.WareProperty;
import org.junit.Test;

import static org.junit.Assert.*;

public class PropertyTest {

    @Test
    public void addProperty() {
        Property property = new Property();
        WareProperty wareProperty = new WareProperty();

        property.addProperty(wareProperty);
        assertEquals(1, property.getWareProperties().size());
        assertTrue(property.getWareProperties().contains(wareProperty));
    }
}