package com.andrey.warehouse.entities.property;

import com.andrey.warehouse.exceptions.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class PropertyControllerImplTest {

    @Mock
    private PropertyService service;
    private PropertyController controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        controller = new PropertyControllerImpl(service);
    }

    @Test
    public void updateProperty() {
        PropertyDto propertyDto = new PropertyDto();
        propertyDto.setId(1L);

        when(service.updateProperty(propertyDto)).thenReturn(propertyDto);

        assertEquals(propertyDto, controller.updateProperty(propertyDto));
        verify(service, times(1)).updateProperty(propertyDto);
    }

    @Test(expected = NotFoundException.class)
    public void updatePropertyNullId() {
        controller.updateProperty(new PropertyDto());
    }

    @Test
    public void getPropertyByName() {
        PropertyDto propertyDto = new PropertyDto();
        when(service.findPropertyDtoByName("property")).thenReturn(propertyDto);

        assertEquals(propertyDto, controller.getPropertyByName("property"));
        verify(service, times(1)).findPropertyDtoByName("property");
    }

    @Test
    public void getPropertyById() {
        PropertyDto propertyDto = new PropertyDto();
        when(service.findPropertyDtoById(1L)).thenReturn(propertyDto);

        assertEquals(propertyDto, controller.getPropertyById(1L));
        verify(service, times(1)).findPropertyDtoById(1L);
    }

    @Test
    public void getProperties() {
        PropertyDto propertyDto1 = new PropertyDto();
        propertyDto1.setId(1L);
        propertyDto1.setName("1");
        PropertyDto propertyDto2 = new PropertyDto();
        propertyDto2.setId(2L);
        propertyDto2.setName("2");
        PropertyDto propertyDto3 = new PropertyDto();
        propertyDto3.setId(3L);
        propertyDto3.setName("3");

        Set<PropertyDto> properties = Stream.of(propertyDto1, propertyDto2, propertyDto3).collect(Collectors.toSet());

        when(service.findProperties()).thenReturn(properties);

        Set<PropertyDto> result = controller.getProperties();

        assertEquals(properties, result);
        assertEquals(3, result.size());
        verify(service, times(1)).findProperties();
    }

    @Test
    public void addProperty() {
        PropertyDto property = new PropertyDto();

        when(service.saveProperty(property)).thenReturn(property);

        assertEquals(property, controller.addProperty(property));
        verify(service, times(1)).saveProperty(property);
    }
}