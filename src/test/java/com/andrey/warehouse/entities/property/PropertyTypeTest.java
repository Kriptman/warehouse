package com.andrey.warehouse.entities.property;

import org.junit.Test;

import static org.junit.Assert.*;

public class PropertyTypeTest {

    private PropertyType propertyType;

    @Test
    public void checkStringValue() {
        propertyType = PropertyType.STRING;
        assertTrue(propertyType.checkValue("string"));
    }

    @Test
    public void checkBooleanValue() {
        propertyType = PropertyType.BOOLEAN;
        assertTrue(propertyType.checkValue("true"));
    }

    @Test
    public void checkBooleanValueWrong() {
        propertyType = PropertyType.BOOLEAN;
        assertFalse(propertyType.checkValue("not boolean"));
    }

    @Test
    public void checkIntegerValue() {
        propertyType = PropertyType.INTEGER;
        assertTrue(propertyType.checkValue("123"));
    }

    @Test
    public void checkIntegerValueWrong() {
        propertyType = PropertyType.INTEGER;
        assertFalse(propertyType.checkValue("123.11"));
    }

    @Test
    public void checkCurrencyValue() {
        propertyType = PropertyType.CURRENCY;
        assertTrue(propertyType.checkValue("123,23 руб."));
    }

    @Test
    public void checkCurrencyValueWrong() {
        propertyType = PropertyType.CURRENCY;
        assertFalse(propertyType.checkValue("123.11 руб."));
    }

    @Test
    public void checkDoubleValue() {
        propertyType = PropertyType.DOUBLE;
        assertTrue(propertyType.checkValue("123.4"));
    }

    @Test
    public void checkDoubleValueWrong() {
        propertyType = PropertyType.DOUBLE;
        assertFalse(propertyType.checkValue("123,4"));
    }
}