package com.andrey.warehouse.entities.ware;

import com.andrey.warehouse.entities.wareProperty.WareProperty;
import org.junit.Test;

import static org.junit.Assert.*;

public class WareTest {

    @Test
    public void addProperty() {
        Ware ware = new Ware();
        WareProperty wareProperty = new WareProperty();

        ware.addProperty(wareProperty);
        assertEquals(1, ware.getWareProperties().size());
        assertTrue(ware.getWareProperties().contains(wareProperty));
    }
}