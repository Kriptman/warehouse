package com.andrey.warehouse.entities.ware;

import com.andrey.warehouse.entities.group.Group;
import com.andrey.warehouse.entities.group.GroupService;
import com.andrey.warehouse.exceptions.InternalException;
import com.andrey.warehouse.exceptions.NotFoundException;
import com.andrey.warehouse.utils.Setup;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

public class WareServiceImplTest {

    @Mock
    WareRepository repository;
    @Mock
    WareMapper mapper;
    @Mock
    GroupService groupService;

    private WareService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        service = new WareServiceImpl(repository, groupService, mapper);
    }

    @Test
    public void findById() {
        Ware ware = new Ware();
        ware.setId(1L);

        when(repository.findById(1L)).thenReturn(Optional.of(ware));

        assertEquals(ware, service.findById(1L));
        verify(repository, times(1)).findById(1L);
    }

    @Test(expected = NotFoundException.class)
    public void findByIdNotFound() {
        when(repository.findById(1L)).thenReturn(Optional.empty());
        service.findById(1L);
    }

    @Test
    public void saveWareDto() {
        Ware ware = new Ware();
        ware.setName("Name");
        WareDto dto = new WareDto();
        dto.setName("Name");
        Group group = new Group();

        when(mapper.toWare(dto)).thenReturn(ware);
        when(repository.save(ware)).thenReturn(ware);
        when(mapper.toWareDto(ware)).thenReturn(dto);
        when(groupService.findByIdWithWares(0L)).thenReturn(group);

        assertEquals(dto, service.saveWareDto(dto, 0L));
        verify(mapper, times(1)).toWare(dto);
        verify(repository, times(1)).save(ware);
        verify(mapper, times(1)).toWareDto(ware);
        verify(groupService, times(1)).findByIdWithWares(0L);
    }

    @Test
    public void findWareDtoById() {
        Ware ware = new Ware();
        ware.setId(1L);
        WareDto wareDto = new WareDto();
        wareDto.setId(1L);

        when(mapper.toWareDto(ware)).thenReturn(wareDto);
        when(repository.findById(1L)).thenReturn(Optional.of(ware));

        assertEquals(wareDto, service.findWareDtoById(1L));
        verify(mapper, times(1)).toWareDto(ware);
    }

    @Test
    public void deleteById() {
        Ware ware = new Ware();
        ware.setStatus(WareStatus.ACTIVE);

        when(repository.findById(1L)).thenReturn(Optional.of(ware));
        ArgumentCaptor<Ware> argumentCaptor = ArgumentCaptor.forClass(Ware.class);

        service.deleteById(1L);

        verify(repository, times(1)).findById(1L);
        verify(repository, times(1)).save(argumentCaptor.capture());
        verify(repository, never()).deleteById(1L);
        Ware savedWare = argumentCaptor.getValue();
        assertEquals(WareStatus.DELETED, savedWare.getStatus());
    }

    @Test(expected = NotFoundException.class)
    public void deleteByIdNotFound() {
        when(repository.findById(1L)).thenReturn(Optional.empty());

        service.deleteById(1L);
    }

    @Test
    public void findWares() {
        Ware ware = new Ware();
        ware.setId(1L);
        ware.setName("1");
        Ware ware2 = new Ware();
        ware2.setId(2L);
        ware2.setName("2");
        Ware ware3 = new Ware();
        ware3.setId(3L);
        ware3.setName("3");
        List<Ware> wares = Stream.of(ware, ware2, ware3).collect(Collectors.toList());

        WareDto wareDto = new WareDto();
        wareDto.setId(1L);
        wareDto.setName("1");
        WareDto wareDto2 = new WareDto();
        wareDto2.setId(2L);
        wareDto2.setName("2");
        WareDto wareDto3 = new WareDto();
        wareDto3.setId(3L);
        wareDto3.setName("3");
        Set<WareDto> wareDtoSet = Stream.of(wareDto, wareDto2, wareDto3).collect(Collectors.toSet());

        when(repository.findAll()).thenReturn(wares);
        when(mapper.toWareDtoSet(wares)).thenReturn(wareDtoSet);

        Set<WareDto> result = service.findWares();
        assertEquals(3, result.size());
        verify(repository, times(1)).findAll();
        verify(mapper, times(1)).toWareDtoSet(wares);
    }

    @Test
    public void findWaresByStatus() {
        Ware ware = new Ware();
        ware.setId(1L);
        ware.setName("1");
        Ware ware2 = new Ware();
        ware2.setId(2L);
        ware2.setName("2");
        Ware ware3 = new Ware();
        ware3.setId(3L);
        ware3.setName("3");
        Set<Ware> wares = Stream.of(ware, ware2, ware3).collect(Collectors.toSet());

        WareDto wareDto = new WareDto();
        wareDto.setId(1L);
        wareDto.setName("1");
        WareDto wareDto2 = new WareDto();
        wareDto2.setId(2L);
        wareDto2.setName("2");
        WareDto wareDto3 = new WareDto();
        wareDto3.setId(3L);
        wareDto3.setName("3");
        Set<WareDto> wareDtoSet = Stream.of(wareDto, wareDto2, wareDto3).collect(Collectors.toSet());

        when(repository.findAllByStatus(WareStatus.ACTIVE)).thenReturn(wares);
        when(mapper.toWareDtoSet(wares)).thenReturn(wareDtoSet);

        Set<WareDto> result = service.findWaresByStatus(WareStatus.ACTIVE);
        assertEquals(3, result.size());
        verify(repository, times(1)).findAllByStatus(WareStatus.ACTIVE);
        verify(mapper, times(1)).toWareDtoSet(wares);
    }

    @Test
    public void findByName() {
        Ware ware = new Ware();
        ware.setName("1");

        when(repository.findFirstByName("1")).thenReturn(Optional.of(ware));

        assertEquals(ware, service.findByName("1"));
        verify(repository, times(1)).findFirstByName("1");
    }

    @Test(expected = NotFoundException.class)
    public void findByNameNotFound() {
        when(repository.findFirstByName("1")).thenReturn(Optional.empty());
        service.findByName("1");
    }

    @Test
    public void findWareDtoByName() {
        Ware ware = new Ware();
        ware.setName("1");
        WareDto wareDto = new WareDto();

        when(mapper.toWareDto(ware)).thenReturn(wareDto);
        when(repository.findFirstByName("1")).thenReturn(Optional.of(ware));

        assertEquals(wareDto, service.findWareDtoByName("1"));
        verify(mapper, times(1)).toWareDto(ware);
    }

    @Test
    public void updateWareDto() {
        Ware ware = Setup.setupWare();
        WareDto wareDto = Setup.setupWareDto();

        when(repository.findById(1L)).thenReturn(Optional.of(ware));
        when(repository.save(ware)).thenReturn(ware);
        when(mapper.toWareDto(ware)).thenReturn(wareDto);
        when(mapper.toWare(wareDto)).thenReturn(ware);

        assertEquals(wareDto, service.updateWareDto(wareDto));
        verify(repository, times(1)).findById(1L);
        verify(repository, times(1)).save(ware);
        verify(mapper, times(1)).toWareDto(ware);
        verify(mapper, times(1)).toWare(wareDto);
    }

    @Test
    public void updateWareDtoWithDetail() {
        Ware wareFrom = Setup.setupWare();
        Ware wareTo = Setup.setupWare();
        wareTo.setName("New Name");
        wareTo.getWareDetail().setQuantity(5);
        WareDto wareDto = Setup.setupWareDto();
        wareDto.setName("New Name");
        wareDto.getWareDetail().setQuantity(5);

        when(repository.findById(1L)).thenReturn(Optional.of(wareFrom));
        when(repository.save(wareTo)).thenReturn(wareTo);
        when(mapper.toWareDto(wareTo)).thenReturn(wareDto);
        when(mapper.toWare(wareDto)).thenReturn(wareTo);

        assertEquals(wareDto, service.updateWareDto(wareDto));
        verify(repository, times(1)).findById(1L);
        verify(repository, times(1)).save(wareTo);
        verify(mapper, times(1)).toWareDto(wareTo);
        verify(mapper, times(1)).toWare(wareDto);
    }

    @Test(expected = NotFoundException.class)
    public void updateWareDtoNotFound() {
        WareDto dto = new WareDto();
        dto.setId(1L);
        when(repository.findById(1L)).thenReturn(Optional.empty());

        service.updateWareDto(dto);
    }

    @Test(expected = InternalException.class)
    public void updateWareDtoInternalException() {
        WareDto dto = new WareDto();
        dto.setId(1L);
        Ware ware = new Ware();
        when(repository.findById(1L)).thenReturn(Optional.of(ware));
        when(mapper.toWare(dto)).thenReturn(null);

        service.updateWareDto(dto);
    }

    @Test
    public void updateWareDtoNull() {
        assertNull(service.updateWareDto(null));
    }

    @Test
    public void findWaresWithPages() {
        Pageable pageable = PageRequest.of(0, 3);
        Ware ware = new Ware();
        Ware ware2 = new Ware();
        Ware ware3 = new Ware();
        List<Ware> wareList = Stream.of(ware, ware2, ware3).collect(Collectors.toList());
        Page<Ware> page = new PageImpl<>(wareList, pageable, 6);
        WareDto dto = new WareDto();
        WareDto dto2 = new WareDto();
        WareDto dto3 = new WareDto();
        List<WareDto> wareDtoList = Stream.of(dto, dto2, dto3).collect(Collectors.toList());

        when(repository.findAll(pageable)).thenReturn(page);
        when(mapper.toWareDtoList(wareList)).thenReturn(wareDtoList);

        Page<WareDto> dtoPage = service.findWaresWithPages(pageable);

        assertEquals(3, dtoPage.getSize());
        assertEquals(6, dtoPage.getTotalElements());
        assertEquals(2, dtoPage.getTotalPages());
        assertEquals(0, dtoPage.getNumber());
        verify(repository, times(1)).findAll(pageable);
        verify(mapper, times(1)).toWareDtoList(any());
    }

    @Test
    public void moveWareToGroup() {
        Ware ware = Setup.setupWare();
        ware.setGroup(null);
        Group group = Setup.setupGroup();

        when(repository.findById(1L)).thenReturn(Optional.of(ware));
        when(groupService.findByIdWithWares(1L)).thenReturn(group);
        when(repository.save(ware)).thenReturn(ware);

        ArgumentCaptor<Ware> wareArgumentCaptor = ArgumentCaptor.forClass(Ware.class);

        service.moveWareToGroup(1L, 1L);

        verify(repository).save(wareArgumentCaptor.capture());
        assertEquals(Setup.setupWare(), wareArgumentCaptor.getValue());
        verify(repository, times(1)).findById(1L);
        verify(groupService, times(1)).findByIdWithWares(1L);
        verify(repository, times(1)).save(ware);
    }
}