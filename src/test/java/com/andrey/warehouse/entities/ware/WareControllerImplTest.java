package com.andrey.warehouse.entities.ware;

import com.andrey.warehouse.entities.wareDetail.WareDetailDto;
import com.andrey.warehouse.exceptions.NotFoundException;
import com.andrey.warehouse.utils.Setup;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class WareControllerImplTest {

    @Mock
    WareService service;

    private WareController controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        controller = new WareControllerImpl(service);
    }

    @Test
    public void addWare() {
        WareDto ware = Setup.setupWareDto();

        when(service.saveWareDto(ware, 0L)).thenReturn(ware);

        assertEquals(ware, controller.addWare(ware, 0L));
        verify(service, times(1)).saveWareDto(ware, 0L);
    }

    @Test
    public void addWareWithDetail() {
        WareDto ware = new WareDto();
        WareDetailDto wareDetailDto = new WareDetailDto();
        ware.setWareDetail(wareDetailDto);

        when(service.saveWareDto(ware, 0L)).thenReturn(ware);

        assertEquals(ware, controller.addWare(ware, 0L));
        verify(service, times(1)).saveWareDto(ware, 0L);
    }

    @Test
    public void getWareByName() {
        WareDto ware = new WareDto();

        when(service.findWareDtoByName("wareName")).thenReturn(ware);

        assertEquals(ware, controller.getWareByName("wareName"));
        verify(service, times(1)).findWareDtoByName("wareName");
    }

    @Test
    public void getWareById() {
        WareDto ware = new WareDto();

        when(service.findWareDtoById(1L)).thenReturn(ware);

        assertEquals(ware, controller.getWareById(1L));
        verify(service, times(1)).findWareDtoById(1L);
    }

    @Test
    public void getWares() {
        WareDto ware = new WareDto();
        ware.setName("1");
        WareDto ware2 = new WareDto();
        ware2.setName("2");
        WareDto ware3 = new WareDto();
        ware3.setName("3");
        Set<WareDto> wareList = Stream.of(ware, ware2, ware3).collect(Collectors.toSet());

        when(service.findWares()).thenReturn(wareList);

        Set<WareDto> result = controller.getWares();

        assertEquals(wareList, result);
        assertEquals(3, result.size());
        verify(service, times(1)).findWares();
    }

    @Test
    public void updateWare() {
        WareDto dto = new WareDto();
        dto.setId(1L);

        when(service.updateWareDto(dto)).thenReturn(dto);

        assertEquals(dto, controller.updateWare(dto));
        verify(service, times(1)).updateWareDto(dto);
    }

    @Test(expected = NotFoundException.class)
    public void updateWareNotFound() {
        WareDto dto = new WareDto();

        controller.updateWare(dto);
    }

    @Test
    public void deleteById() {
        doNothing().when(service).deleteById(1L);

        controller.deleteById(1L);
        verify(service, times(1)).deleteById(1L);
    }

    @Test
    public void getWaresByStatus() {
        WareDto ware = new WareDto();
        ware.setName("1");
        WareDto ware2 = new WareDto();
        ware2.setName("2");
        WareDto ware3 = new WareDto();
        ware3.setName("3");
        Set<WareDto> wareList = Stream.of(ware, ware2, ware3).collect(Collectors.toSet());

        when(service.findWaresByStatus(WareStatus.DELETED)).thenReturn(wareList);

        Set<WareDto> result = controller.getWaresByStatus(WareStatus.DELETED);

        assertEquals(wareList, result);
        assertEquals(3, result.size());
        verify(service, times(1)).findWaresByStatus(WareStatus.DELETED);
    }

    @Test
    public void getWaresWithPages() {
        WareDto ware = new WareDto();
        ware.setName("1");
        WareDto ware2 = new WareDto();
        ware2.setName("2");
        WareDto ware3 = new WareDto();
        ware3.setName("3");
        List<WareDto> wareList = Stream.of(ware, ware2, ware3).collect(Collectors.toList());
        PageRequest pageable = PageRequest.of(0, 3);
        PageImpl<WareDto> page = new PageImpl<>(wareList, pageable, 6);

        when(service.findWaresWithPages(pageable)).thenReturn(page);

        WareDtoPage dtoPage = controller.getWaresWithPages(0, 3);

        assertEquals(3L, dtoPage.getWares().size());
        assertEquals(2, dtoPage.getTotalPages());
        assertEquals(6, dtoPage.getTotalItems().intValue());
        verify(service, times(1)).findWaresWithPages(pageable);
    }

    @Test
    public void moveWareToGroup() {
        doNothing().when(service).moveWareToGroup(1L, 1L);

        controller.moveWareToGroup(1L, 1L);
        verify(service, times(1)).moveWareToGroup(1L, 1L);
    }
}