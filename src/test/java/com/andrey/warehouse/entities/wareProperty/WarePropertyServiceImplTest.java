package com.andrey.warehouse.entities.wareProperty;

import com.andrey.warehouse.entities.property.Property;
import com.andrey.warehouse.entities.property.PropertyService;
import com.andrey.warehouse.entities.property.PropertyType;
import com.andrey.warehouse.entities.ware.Ware;
import com.andrey.warehouse.entities.ware.WareService;
import com.andrey.warehouse.exceptions.InternalException;
import com.andrey.warehouse.exceptions.NotFoundException;
import com.andrey.warehouse.exceptions.WrongValueException;
import com.andrey.warehouse.utils.Setup;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

public class WarePropertyServiceImplTest {

    @Mock
    private WarePropertyRepository repository;
    @Mock
    private WareService wareService;
    @Mock
    private PropertyService propertyService;
    @Mock
    private WarePropertyMapper mapper;

    private WarePropertyService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        service = new WarePropertyServiceImpl(repository, wareService, propertyService, mapper);
    }

    @Test
    public void saveProperty() {
        WareProperty property = Setup.setupWareProperty();
        WarePropertyDto dto = Setup.setupWarePropertyDto();
        Ware ware = Setup.setupWare();
        Property prop = Setup.setupProperty();

        when(mapper.toWarePropertyDto(property)).thenReturn(dto);
        when(mapper.toWareProperty(dto)).thenReturn(property);
        when(wareService.findById(1L)).thenReturn(ware);
        when(propertyService.findById(1L)).thenReturn(prop);
        when(repository.save(property)).thenReturn(property);

        assertEquals(dto, service.saveProperty(dto));
        verify(mapper, times(1)).toWarePropertyDto(property);
        verify(mapper, times(1)).toWareProperty(dto);
        verify(repository, times(1)).save(property);
        verify(wareService, times(1)).findById(1L);
        verify(propertyService, times(1)).findById(1L);
    }

    @Test(expected = WrongValueException.class)
    public void savePropertyWrongValueException() {
        WareProperty property = Setup.setupWareProperty();
        WarePropertyDto dto = Setup.setupWarePropertyDto();
        Ware ware = Setup.setupWare();
        Property prop = Setup.setupProperty();
        prop.setType(PropertyType.DOUBLE);

        when(mapper.toWareProperty(dto)).thenReturn(property);
        when(wareService.findById(1L)).thenReturn(ware);
        when(propertyService.findById(1L)).thenReturn(prop);

        service.saveProperty(dto);
    }

    @Test(expected = InternalException.class)
    public void savePropertyInternalException() {
        WarePropertyDto dto = Setup.setupWarePropertyDto();
        Ware ware = Setup.setupWare();
        Property prop = Setup.setupProperty();

        when(mapper.toWareProperty(dto)).thenReturn(null);
        when(wareService.findById(1L)).thenReturn(ware);
        when(propertyService.findById(1L)).thenReturn(prop);

        service.saveProperty(dto);
    }

    @Test
    public void findDtoById() {
        WarePropertyDto dto = new WarePropertyDto();
        WareProperty property = new WareProperty();
        WarePropertyId id = new WarePropertyId(1L, 1L);

        when(repository.findById(id)).thenReturn(Optional.of(property));
        when(mapper.toWarePropertyDto(property)).thenReturn(dto);

        assertEquals(dto, service.findPropertyDtoById(id));
        verify(mapper, times(1)).toWarePropertyDto(property);
        verify(repository, times(1)).findById(id);
    }

    @Test
    public void findById() {
        WareProperty property = new WareProperty();
        WarePropertyId id = new WarePropertyId(1L, 1L);

        when(repository.findById(id)).thenReturn(Optional.of(property));

        assertEquals(property, service.findById(id));
        verify(repository, times(1)).findById(id);
    }

    @Test(expected = NotFoundException.class)
    public void findByIdNotFound() {
        WarePropertyId id = new WarePropertyId(1L, 1L);
        when(repository.findById(id)).thenReturn(Optional.empty());

        service.findById(id);
    }

    @Test
    public void updatePropertyDto() {
        WareProperty property = Setup.setupWareProperty();
        WarePropertyDto dto = Setup.setupWarePropertyDto();
        WarePropertyId id = new WarePropertyId(1L, 1L);

        when(repository.findById(id)).thenReturn(Optional.of(property));
        when(mapper.toWareProperty(dto)).thenReturn(property);
        when(repository.save(property)).thenReturn(property);
        when(mapper.toWarePropertyDto(property)).thenReturn(dto);

        assertEquals(dto, service.updatePropertyDto(dto));
        verify(mapper, times(1)).toWarePropertyDto(property);
        verify(mapper, times(1)).toWareProperty(dto);
        verify(repository, times(1)).save(property);
        verify(repository, times(1)).findById(id);
    }

    @Test(expected = InternalException.class)
    public void updatePropertyDtoInternalException() {
        WareProperty property = Setup.setupWareProperty();
        WarePropertyDto dto = Setup.setupWarePropertyDto();
        WarePropertyId id = new WarePropertyId(1L, 1L);

        when(repository.findById(id)).thenReturn(Optional.of(property));
        when(mapper.toWareProperty(dto)).thenReturn(null);

        service.updatePropertyDto(dto);
    }

    @Test
    public void updatePropertyDtoNull() {
        assertNull(service.updatePropertyDto(null));
        verify(repository, never()).save(any());
    }

    @Test
    public void findPropertiesByWareId() {
        WarePropertyDto propertyDto1 = new WarePropertyDto();
        propertyDto1.setId(new WarePropertyId(1L, 1L));
        propertyDto1.setValue("1");
        WarePropertyDto propertyDto2 = new WarePropertyDto();
        propertyDto2.setId(new WarePropertyId(1L, 2L));
        propertyDto2.setValue("2");
        WarePropertyDto propertyDto3 = new WarePropertyDto();
        propertyDto3.setId(new WarePropertyId(1L, 3L));
        propertyDto3.setValue("3");

        WareProperty property1 = new WareProperty();
        property1.setId(new WarePropertyId(1L, 1L));
        property1.setValue("1");
        WareProperty property2 = new WareProperty();
        property2.setId(new WarePropertyId(1L, 2L));
        property2.setValue("2");
        WareProperty property3 = new WareProperty();
        property3.setId(new WarePropertyId(1L, 3L));
        property3.setValue("3");

        Set<WareProperty> properties = Stream.of(property1, property2, property3).collect(Collectors.toSet());
        Set<WarePropertyDto> propertyDtoSet = Stream.of(propertyDto1, propertyDto2, propertyDto3).collect(Collectors.toSet());

        when(repository.findAllByIdWareId(1L)).thenReturn(properties);
        when(mapper.toWarePropertyDtoSet(properties)).thenReturn(propertyDtoSet);

        Set<WarePropertyDto> result = service.findPropertiesByWareId(1L);
        assertEquals(3, result.size());
        verify(repository, times(1)).findAllByIdWareId(1L);
        verify(mapper, times(1)).toWarePropertyDtoSet(properties);
    }

    @Test
    public void findPropertiesByPropertyId() {
        WarePropertyDto propertyDto1 = new WarePropertyDto();
        propertyDto1.setId(new WarePropertyId(1L, 1L));
        propertyDto1.setValue("1");
        WarePropertyDto propertyDto2 = new WarePropertyDto();
        propertyDto2.setId(new WarePropertyId(2L, 1L));
        propertyDto2.setValue("2");
        WarePropertyDto propertyDto3 = new WarePropertyDto();
        propertyDto3.setId(new WarePropertyId(3L, 1L));
        propertyDto3.setValue("3");

        WareProperty property1 = new WareProperty();
        property1.setId(new WarePropertyId(1L, 1L));
        property1.setValue("1");
        WareProperty property2 = new WareProperty();
        property2.setId(new WarePropertyId(2L, 1L));
        property2.setValue("2");
        WareProperty property3 = new WareProperty();
        property3.setId(new WarePropertyId(3L, 1L));
        property3.setValue("3");

        Set<WareProperty> properties = Stream.of(property1, property2, property3).collect(Collectors.toSet());
        Set<WarePropertyDto> propertyDtoSet = Stream.of(propertyDto1, propertyDto2, propertyDto3).collect(Collectors.toSet());

        when(repository.findAllByIdPropertyId(1L)).thenReturn(properties);
        when(mapper.toWarePropertyDtoSet(properties)).thenReturn(propertyDtoSet);

        Set<WarePropertyDto> result = service.findPropertiesByPropertyId(1L);
        assertEquals(3, result.size());
        verify(repository, times(1)).findAllByIdPropertyId(1L);
        verify(mapper, times(1)).toWarePropertyDtoSet(properties);
    }

    @Test
    public void findProperties() {
        WarePropertyDto propertyDto1 = new WarePropertyDto();
        propertyDto1.setId(new WarePropertyId(1L, 1L));
        propertyDto1.setValue("1");
        WarePropertyDto propertyDto2 = new WarePropertyDto();
        propertyDto2.setId(new WarePropertyId(1L, 2L));
        propertyDto2.setValue("2");
        WarePropertyDto propertyDto3 = new WarePropertyDto();
        propertyDto3.setId(new WarePropertyId(1L, 3L));
        propertyDto3.setValue("3");

        WareProperty property1 = new WareProperty();
        property1.setId(new WarePropertyId(1L, 1L));
        property1.setValue("1");
        WareProperty property2 = new WareProperty();
        property2.setId(new WarePropertyId(1L, 2L));
        property2.setValue("2");
        WareProperty property3 = new WareProperty();
        property3.setId(new WarePropertyId(1L, 3L));
        property3.setValue("3");

        Set<WareProperty> properties = Stream.of(property1, property2, property3).collect(Collectors.toSet());
        Set<WarePropertyDto> propertyDtoSet = Stream.of(propertyDto1, propertyDto2, propertyDto3).collect(Collectors.toSet());

        when(repository.findAll()).thenReturn(properties);
        when(mapper.toWarePropertyDtoSet(properties)).thenReturn(propertyDtoSet);

        Set<WarePropertyDto> result = service.findProperties();
        assertEquals(3, result.size());
        verify(repository, times(1)).findAll();
        verify(mapper, times(1)).toWarePropertyDtoSet(properties);
    }
}