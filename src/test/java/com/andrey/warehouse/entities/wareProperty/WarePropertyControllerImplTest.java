package com.andrey.warehouse.entities.wareProperty;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class WarePropertyControllerImplTest {

    @Mock
    private WarePropertyService service;
    private WarePropertyController controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        controller = new WarePropertyControllerImpl(service);
    }

    @Test
    public void addProperty() {
        WarePropertyDto property = new WarePropertyDto();

        when(service.saveProperty(property)).thenReturn(property);

        assertEquals(property, controller.addProperty(property));
        verify(service, times(1)).saveProperty(property);
    }

    @Test
    public void updateProperty() {
        WarePropertyDto propertyDto = new WarePropertyDto();
        propertyDto.setId(new WarePropertyId(1L, 1L));

        when(service.updatePropertyDto(propertyDto)).thenReturn(propertyDto);

        assertEquals(propertyDto, controller.updateProperty(propertyDto));
        verify(service, times(1)).updatePropertyDto(propertyDto);
    }

    @Test
    public void getPropertiesByWareId() {
        WarePropertyDto propertyDto1 = new WarePropertyDto();
        propertyDto1.setId(new WarePropertyId(1L, 1L));
        propertyDto1.setValue("1");
        WarePropertyDto propertyDto2 = new WarePropertyDto();
        propertyDto2.setId(new WarePropertyId(1L, 2L));
        propertyDto2.setValue("2");
        WarePropertyDto propertyDto3 = new WarePropertyDto();
        propertyDto3.setId(new WarePropertyId(1L, 3L));
        propertyDto3.setValue("3");

        Set<WarePropertyDto> properties = Stream.of(propertyDto1, propertyDto2, propertyDto3).collect(Collectors.toSet());

        when(service.findPropertiesByWareId(1L)).thenReturn(properties);

        Set<WarePropertyDto> result = controller.getPropertiesByWareId(1L);

        assertEquals(properties, result);
        assertEquals(3, result.size());
        verify(service, times(1)).findPropertiesByWareId(1L);
    }

    @Test
    public void getPropertiesByPropertyId() {
        WarePropertyDto propertyDto1 = new WarePropertyDto();
        propertyDto1.setId(new WarePropertyId(1L, 1L));
        propertyDto1.setValue("1");
        WarePropertyDto propertyDto2 = new WarePropertyDto();
        propertyDto2.setId(new WarePropertyId(2L, 1L));
        propertyDto2.setValue("2");
        WarePropertyDto propertyDto3 = new WarePropertyDto();
        propertyDto3.setId(new WarePropertyId(3L, 1L));
        propertyDto3.setValue("3");

        Set<WarePropertyDto> properties = Stream.of(propertyDto1, propertyDto2, propertyDto3).collect(Collectors.toSet());

        when(service.findPropertiesByPropertyId(1L)).thenReturn(properties);

        Set<WarePropertyDto> result = controller.getPropertiesByPropertyId(1L);

        assertEquals(properties, result);
        assertEquals(3, result.size());
        verify(service, times(1)).findPropertiesByPropertyId(1L);
    }

    @Test
    public void getProperties() {
        WarePropertyDto propertyDto1 = new WarePropertyDto();
        propertyDto1.setId(new WarePropertyId(1L, 1L));
        propertyDto1.setValue("1");
        WarePropertyDto propertyDto2 = new WarePropertyDto();
        propertyDto2.setId(new WarePropertyId(2L, 1L));
        propertyDto2.setValue("2");
        WarePropertyDto propertyDto3 = new WarePropertyDto();
        propertyDto3.setId(new WarePropertyId(3L, 1L));
        propertyDto3.setValue("3");

        Set<WarePropertyDto> properties = Stream.of(propertyDto1, propertyDto2, propertyDto3).collect(Collectors.toSet());

        when(service.findProperties()).thenReturn(properties);

        Set<WarePropertyDto> result = controller.getProperties();

        assertEquals(properties, result);
        assertEquals(3, result.size());
        verify(service, times(1)).findProperties();
    }

    @Test
    public void getPropertyById() {
        WarePropertyId id = new WarePropertyId(1L, 1L);
        WarePropertyDto dto = new WarePropertyDto();

        when(service.findPropertyDtoById(id)).thenReturn(dto);

        assertEquals(dto, controller.getPropertyById(id));
        verify(service, times(1)).findPropertyDtoById(id);
    }
}