package com.andrey.warehouse.entities.group;

import com.andrey.warehouse.exceptions.InternalException;
import com.andrey.warehouse.exceptions.NotFoundException;
import com.andrey.warehouse.utils.Setup;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

public class GroupServiceImplTest {

    @Mock
    private GroupRepository repository;
    @Mock
    private GroupMapper mapper;
    @Mock
    private EntityManager entityManager;

    private GroupService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        service = new GroupServiceImpl(repository, mapper, entityManager);
    }

    @Test
    public void findGroupDtoById() {
        Group group = new Group();
        group.setId(1L);
        GroupDto groupDto = new GroupDto();
        groupDto.setId(1L);
        Query query = mock(Query.class);

        when(mapper.toGroupDto(group)).thenReturn(groupDto);
        when(entityManager.createNamedQuery(Group.QUERY_ALL_FIELDS_BY_ID)).thenReturn(query);
        when(entityManager.find(Group.class, 1L)).thenReturn(group);

        assertEquals(groupDto, service.findGroupDtoById(1L));
        verify(mapper, times(1)).toGroupDto(group);
        verify(entityManager, times(1)).find(Group.class, 1L);
    }

    @Test
    public void moveToGroup() {
        Group parent = Setup.setupGroup();
        Group child = new Group();
        child.setId(2L);
        child.setName("child");
        Query query = mock(Query.class);

        when(entityManager.find(Group.class, 1L)).thenReturn(parent);
        when(entityManager.find(Group.class, 2L)).thenReturn(child);
        when(entityManager.createNamedQuery(Group.QUERY_ALL_FIELDS_BY_ID)).thenReturn(query);
        when(repository.save(child)).thenReturn(child);

        ArgumentCaptor<Group> groupArgumentCaptor = ArgumentCaptor.forClass(Group.class);

        service.moveToGroup(2L, 1L);
        verify(repository).save(groupArgumentCaptor.capture());

        Group result = groupArgumentCaptor.getValue();
        assertEquals(child, result);
        assertEquals(parent, result.getParent());
        verify(entityManager, times(1)).find(Group.class,1L);
        verify(entityManager, times(1)).find(Group.class,2L);
        verify(repository, times(1)).save(child);
    }

    @Test
    public void findGroupByName() {
        Group group = new Group();
        group.setId(1L);

        when(repository.findFirstByName("name")).thenReturn(Optional.of(group));

        assertEquals(group, service.findGroupByName("name"));
        verify(repository, times(1)).findFirstByName("name");
    }

    @Test(expected = NotFoundException.class)
    public void findGroupByNameNotFound() {
        when(repository.findFirstByName("name")).thenReturn(Optional.empty());

        service.findGroupByName("name");
    }

    @Test
    public void findGroupDtoByName() {
        Group group = new Group();
        group.setId(1L);
        GroupDto groupDto = new GroupDto();
        groupDto.setId(1L);

        when(mapper.toGroupDto(group)).thenReturn(groupDto);
        when(repository.findFirstByName("name")).thenReturn(Optional.of(group));

        assertEquals(groupDto, service.findGroupDtoByName("name"));
        verify(mapper, times(1)).toGroupDto(group);
    }

    @Test
    public void findGroups() {
        Group group = Setup.setupGroup();
        GroupDto groupDto = Setup.setupGroupDto();
        Query query = mock(Query.class);

        when(mapper.toGroupDto(group)).thenReturn(groupDto);
        when(entityManager.createNamedQuery(Group.QUERY_ALL_FIELDS_BY_ID)).thenReturn(query);
        when(entityManager.find(Group.class, Group.ROOT_ID)).thenReturn(group);

        assertEquals(groupDto, service.findGroups());
        verify(mapper, times(1)).toGroupDto(group);
        verify(entityManager, times(1)).find(Group.class, Group.ROOT_ID);
    }

    @Test
    public void findByIdWithWares() {
        Group group = new Group();
        group.setId(1L);
        Query query = mock(Query.class);

        when(entityManager.createNamedQuery(Group.QUERY_ALL_FIELDS_BY_ID)).thenReturn(query);
        when(entityManager.find(Group.class, 1L)).thenReturn(group);

        assertEquals(group, service.findByIdWithWares(1L));
        verify(entityManager, times(1)).find(Group.class, 1L);
    }

    @Test(expected = NotFoundException.class)
    public void findByIdNotFound() {
        Query query = mock(Query.class);

        when(entityManager.createNamedQuery(Group.QUERY_ALL_FIELDS_BY_ID)).thenReturn(query);
        when(entityManager.find(Group.class, 1L)).thenReturn(null);

        service.findByIdWithWares(1L);
    }

    @Test
    public void saveGroup() {
        Group group = Setup.setupGroup();
        Group parent = new Group();
        parent.setId(2L);
        GroupDto groupDto = Setup.setupGroupDto();
        Query query = mock(Query.class);

        when(mapper.toGroupDto(group)).thenReturn(groupDto);
        when(mapper.toGroup(groupDto)).thenReturn(group);
        when(repository.save(group)).thenReturn(group);
        when(entityManager.createNamedQuery(Group.QUERY_ALL_FIELDS_BY_ID)).thenReturn(query);
        when(entityManager.find(Group.class, 2L)).thenReturn(parent);

        assertEquals(groupDto, service.saveGroup(groupDto, 2L));
        verify(mapper, times(1)).toGroupDto(group);
        verify(mapper, times(1)).toGroup(groupDto);
        verify(repository, times(1)).save(group);
        verify(entityManager, times(1)).find(Group.class, 2L);
    }

    @Test(expected = InternalException.class)
    public void saveGroupInternalException() {
        GroupDto groupDto = Setup.setupGroupDto();

        when(mapper.toGroup(groupDto)).thenReturn(null);

        service.saveGroup(groupDto, 2L);
    }

    @Test(expected = NotFoundException.class)
    public void saveGroupParentNotFound() {
        Group group = Setup.setupGroup();
        GroupDto groupDto = Setup.setupGroupDto();
        Query query = mock(Query.class);

        when(mapper.toGroup(groupDto)).thenReturn(group);
        when(entityManager.createNamedQuery(Group.QUERY_ALL_FIELDS_BY_ID)).thenReturn(query);
        when(entityManager.find(Group.class, 2L)).thenReturn(null);

        service.saveGroup(groupDto, 2L);
    }

    @Test
    public void updateGroup() {
        Group group = Setup.setupGroup();
        GroupDto groupDto = Setup.setupGroupDto();
        Query query = mock(Query.class);

        when(repository.save(group)).thenReturn(group);
        when(mapper.toGroupDto(group)).thenReturn(groupDto);
        when(entityManager.createNamedQuery(Group.QUERY_ALL_FIELDS_BY_ID)).thenReturn(query);
        when(entityManager.find(Group.class, 1L)).thenReturn(group);

        assertEquals(groupDto, service.updateGroup(groupDto));
        verify(mapper, times(1)).toGroupDto(group);
        verify(entityManager, times(1)).find(Group.class, 1L);
        verify(repository, times(1)).save(group);
    }

    @Test
    public void updateGroupNull() {
        assertNull(service.updateGroup(null));
    }
}