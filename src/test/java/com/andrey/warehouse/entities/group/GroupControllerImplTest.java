package com.andrey.warehouse.entities.group;

import com.andrey.warehouse.exceptions.NotFoundException;
import com.andrey.warehouse.utils.Setup;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class GroupControllerImplTest {

    @Mock
    GroupService service;

    private GroupController controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        controller = new GroupControllerImpl(service);
    }

    @Test
    public void addGroup() {
        GroupDto groupDto = Setup.setupGroupDto();

        when(service.saveGroup(groupDto, 1L)).thenReturn(groupDto);

        assertEquals(groupDto, controller.addGroup(groupDto, 1L));
        verify(service, times(1)).saveGroup(groupDto, 1L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addGroupNullParentId() {
        GroupDto groupDto = Setup.setupGroupDto();

        controller.addGroup(groupDto, null);
    }

    @Test
    public void updateGroup() {
        GroupDto groupDto = Setup.setupGroupDto();

        when(service.updateGroup(groupDto)).thenReturn(groupDto);

        assertEquals(groupDto, controller.updateGroup(groupDto));
        verify(service, times(1)).updateGroup(groupDto);
    }

    @Test(expected = NotFoundException.class)
    public void updateGroupNotFound() {
        GroupDto groupDto = new GroupDto();
        controller.updateGroup(groupDto);
    }

    @Test
    public void getGroupById() {
        GroupDto groupDto = Setup.setupGroupDto();

        when(service.findGroupDtoById(1L)).thenReturn(groupDto);

        assertEquals(groupDto, controller.getGroupById(1L));
        verify(service, times(1)).findGroupDtoById(1L);
    }

    @Test
    public void getGroupByName() {
        GroupDto groupDto = Setup.setupGroupDto();

        when(service.findGroupDtoByName("name")).thenReturn(groupDto);

        assertEquals(groupDto, controller.getGroupByName("name"));
        verify(service, times(1)).findGroupDtoByName("name");
    }

    @Test
    public void moveToGroup() {
        doNothing().when(service).moveToGroup(1L, 1L);

        controller.moveToGroup(1L, 1L);
        verify(service, times(1)).moveToGroup(1L, 1L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void moveToGroupNullParentId() {
        controller.moveToGroup(1L, null);
    }

    @Test
    public void getGroups() {
        GroupDto groupDto1 = new GroupDto();
        groupDto1.setName("1");
        GroupDto groupDto2 = new GroupDto();
        groupDto2.setName("2");
        GroupDto groupDto3 = new GroupDto();
        groupDto3.setName("3");
        groupDto1.getChildes().add(groupDto2);
        groupDto1.getChildes().add(groupDto3);

        when(service.findGroups()).thenReturn(groupDto1);

        GroupDto result = controller.getGroups();

        assertEquals(2, result.getChildes().size());
        verify(service, times(1)).findGroups();
        assertTrue(result.getChildes().containsAll(Stream.of(groupDto2, groupDto3).collect(Collectors.toSet())));
    }
}