package com.andrey.warehouse.entities.group;

import com.andrey.warehouse.entities.ware.Ware;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GroupTest {

    @Test
    public void addChild() {
        Group group = new Group();
        Group child = new Group();

        group.addChild(child);
        assertEquals(1, group.getChildes().size());
        assertTrue(group.getChildes().contains(child));
    }

    @Test
    public void create() {
        Group parent = new Group();
        parent.setName("parent");
        parent.setId(1L);
        Group child = new Group("name", parent);

        assertEquals(1, parent.getChildes().size());
        assertTrue(parent.getChildes().contains(child));
        assertEquals(parent, child.getParent());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createNullParent() {
        new Group("name", null);
    }

    @Test
    public void addWare() {
        Ware ware = new Ware();
        Group group = new Group();

        group.addWare(ware);

        assertEquals(1, group.getWares().size());
        assertEquals(group, ware.getGroup());
    }
}