package com.andrey.warehouse.utils;

import com.andrey.warehouse.entities.group.Group;
import com.andrey.warehouse.entities.group.GroupDto;
import com.andrey.warehouse.entities.property.Property;
import com.andrey.warehouse.entities.property.PropertyDto;
import com.andrey.warehouse.entities.property.PropertyType;
import com.andrey.warehouse.entities.ware.Ware;
import com.andrey.warehouse.entities.ware.WareDto;
import com.andrey.warehouse.entities.ware.WareStatus;
import com.andrey.warehouse.entities.wareDetail.WareDetail;
import com.andrey.warehouse.entities.wareDetail.WareDetailDto;
import com.andrey.warehouse.entities.wareProperty.WareProperty;
import com.andrey.warehouse.entities.wareProperty.WarePropertyDto;
import com.andrey.warehouse.entities.wareProperty.WarePropertyId;

public class Setup {

    public static Ware setupWare() {
        Ware ware = new Ware();
        ware.setId(1L);
        ware.setName("name");
        ware.setNds(11);
        ware.setShortName("short");
        ware.setStatus(WareStatus.ACTIVE);
        ware.setUseWareNds(true);

        WareDetail wareDetail = setupWareDetail();
        wareDetail.setWare(ware);
        ware.setWareDetail(wareDetail);

        WareProperty wareProperty = setupWareProperty();
        ware.addProperty(wareProperty);

        Group group = setupGroup();
        group.addWare(ware);

        return ware;
    }

    public static WareDetail setupWareDetail() {
        WareDetail detail = new WareDetail();
        detail.setId(1L);
        detail.setQuantity(1);
        detail.setPrice(10);
        return detail;
    }

    public static WareProperty setupWareProperty() {
        WareProperty wareProperty = new WareProperty();
        wareProperty.setId(new WarePropertyId(1L, 1L));
        wareProperty.setValue("value");
        return wareProperty;
    }

    public static WareDto setupWareDto() {
        WareDto wareDto = new WareDto();
        wareDto.setId(1L);
        wareDto.setName("name");
        wareDto.setNds(11);
        wareDto.setShortName("short");
        wareDto.setStatus(WareStatus.ACTIVE);
        wareDto.setUseWareNds(true);

        WareDetailDto wareDetail = setupWareDetailDto();
        wareDto.setWareDetail(wareDetail);

        WarePropertyDto warePropertyDto = setupWarePropertyDto();
        wareDto.getWareProperties().add(warePropertyDto);

        return wareDto;
    }

    public static WareDetailDto setupWareDetailDto() {
        WareDetailDto detailDto = new WareDetailDto();
        detailDto.setId(1L);
        detailDto.setQuantity(1);
        detailDto.setPrice(10);
        return detailDto;
    }

    public static WarePropertyDto setupWarePropertyDto() {
        WarePropertyDto warePropertyDto = new WarePropertyDto();
        warePropertyDto.setId(new WarePropertyId(1L, 1L));
        warePropertyDto.setValue("value");
        return warePropertyDto;
    }

    public static Property setupProperty() {
        Property property = new Property();
        property.setId(1L);
        property.setName("Property");
        property.setType(PropertyType.STRING);
        return property;
    }

    public static PropertyDto setupPropertyDto() {
        PropertyDto property = new PropertyDto();
        property.setId(1L);
        property.setName("Property");
        property.setType(PropertyType.STRING);
        return property;
    }

    public static Group setupGroup() {
        Group group = new Group();
        group.setId(1L);
        group.setName("Group");
        group.setParent(null);
        return group;
    }

    public static GroupDto setupGroupDto() {
        GroupDto group = new GroupDto();
        group.setId(1L);
        group.setName("Group");
        return group;
    }
}
