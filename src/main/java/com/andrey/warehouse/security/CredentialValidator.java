package com.andrey.warehouse.security;

import com.andrey.warehouse.utils.LOG;
import javassist.NotFoundException;
import org.apache.wss4j.common.ext.WSSecurityException;
import org.apache.wss4j.dom.handler.RequestData;
import org.apache.wss4j.dom.validate.Credential;
import org.apache.wss4j.dom.validate.UsernameTokenValidator;
import org.springframework.stereotype.Service;

@Service
public class CredentialValidator extends UsernameTokenValidator {

    @Override
    public Credential validate(Credential credential, RequestData data) throws WSSecurityException {
        String user = credential.getUsernametoken().getName();
        String password = credential.getUsernametoken().getPassword();

        try {
            validateUser(user, password);
        } catch (Exception e) {
            LOG.logger().error(e.getMessage());
            throw new WSSecurityException(WSSecurityException.ErrorCode.FAILED_AUTHENTICATION);
        }
        // put here checking authorities and roles
        return credential;
    }

    private void validateUser(String userName, String password) throws NotFoundException {
        // put here loading from db and so on
        if ("user".equals(userName) && "password".equals(password)) {
            return;
        }
        if ("admin".equals(userName) && "password".equals(password)) {
            return;
        }
        throw new NotFoundException("User or password not found");
    }
}
