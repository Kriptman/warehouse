package com.andrey.warehouse.utils;

import com.andrey.warehouse.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LOG {
    private final static Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static Logger logger() {
        return LOGGER;
    }
}
