package com.andrey.warehouse;

import com.andrey.warehouse.entities.group.GroupControllerImpl;
import com.andrey.warehouse.entities.group.GroupService;
import com.andrey.warehouse.entities.property.PropertyControllerImpl;
import com.andrey.warehouse.entities.property.PropertyService;
import com.andrey.warehouse.entities.ware.WareControllerImpl;
import com.andrey.warehouse.entities.ware.WareService;
import com.andrey.warehouse.entities.wareProperty.WarePropertyControllerImpl;
import com.andrey.warehouse.entities.wareProperty.WarePropertyService;
import com.andrey.warehouse.security.CredentialValidator;
import lombok.RequiredArgsConstructor;
import org.apache.cxf.Bus;
import org.apache.cxf.binding.soap.saaj.SAAJInInterceptor;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.apache.cxf.validation.BeanValidationFeature;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.wss4j.dom.WSConstants;
import org.apache.wss4j.dom.handler.WSHandlerConstants;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@Configuration
@RequiredArgsConstructor
public class Application {

    private final ApplicationContext applicationContext;
    private final WareService wareService;
    private final PropertyService propertyService;
    private final GroupService groupService;
    private final WarePropertyService warePropertyService;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public ServletRegistrationBean<CXFServlet> servletRegistrationBean() {
        return new ServletRegistrationBean<>(new CXFServlet(), "/services/*");
    }

    private EndpointImpl createEndpoint(String path, Object controllerImpl) {
        Bus bus = (Bus) applicationContext.getBean(Bus.DEFAULT_BUS_ID);
        EndpointImpl endpoint = new EndpointImpl(bus, controllerImpl);
        endpoint.getFeatures().add(new BeanValidationFeature());

        endpoint.getInInterceptors().add(new SAAJInInterceptor());
        Map<String, Object> props = new HashMap<>();
        props.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
        props.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
        endpoint.getInInterceptors().add(new WSS4JInInterceptor(props));

        endpoint.getProperties().put("ws-security.validate.token", false);
        endpoint.getProperties().put("ws-security.ut.no-callbacks", true);
        endpoint.getProperties().put("ws-security.ut.validator", CredentialValidator.class.getName());

        endpoint.publish(path);
        return endpoint;
    }

    @Bean
    public EndpointImpl wareService() {
        return createEndpoint("/WareController", new WareControllerImpl(wareService));
    }

    @Bean
    public EndpointImpl propertyService() {
        return createEndpoint("/PropertyController", new PropertyControllerImpl(propertyService));
    }

    @Bean
    public EndpointImpl groupService() {
        return createEndpoint("/GroupController", new GroupControllerImpl(groupService));
    }

    @Bean
    public EndpointImpl warePropertyService() {
        return createEndpoint("/WarePropertyController", new WarePropertyControllerImpl(warePropertyService));
    }
}
