package com.andrey.warehouse.entities.ware;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Set;

public interface WareService {

    Ware findById(Long id);

    WareDto saveWareDto(WareDto wareDto, Long groupId);

    WareDto findWareDtoById(Long id);

    void deleteById(Long id);

    Ware findByName(String name);

    WareDto findWareDtoByName(String name);

    WareDto updateWareDto(WareDto wareDto);

    Set<WareDto> findWaresByStatus(WareStatus status);

    Set<WareDto> findWares();

    Page<WareDto> findWaresWithPages(Pageable pageable);

    void moveWareToGroup(Long wareId, Long groupId);
}
