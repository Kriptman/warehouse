package com.andrey.warehouse.entities.ware;

public enum WareStatus {
    ACTIVE,
    INACTIVE,
    DELETED
}
