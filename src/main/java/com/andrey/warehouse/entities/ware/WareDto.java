package com.andrey.warehouse.entities.ware;

import com.andrey.warehouse.entities.wareDetail.WareDetailDto;
import com.andrey.warehouse.entities.wareProperty.WarePropertyDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

@Data
@XmlRootElement(name = "ware")
@XmlAccessorType(XmlAccessType.FIELD)
@ToString
@EqualsAndHashCode
public class WareDto {

    private Long id;

    @NotBlank
    @Size(min = 3, max = 255)
    @NotNull
    @XmlElement(required = true)
    private String name;

    @Size(min = 3, max = 80)
    private String shortName;

    @NotNull
    @Min(0)
    @Max(99)
    @Digits(integer = 2, fraction = 2)
    private double nds;

    private Boolean useWareNds;

    @NotNull
    @XmlElement(required = true)
    private WareStatus status;

    @NotNull
    @XmlElement(required = true)
    private WareDetailDto wareDetail;

    private Set<WarePropertyDto> wareProperties = new HashSet<>();
}

