package com.andrey.warehouse.entities.ware;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.xml.bind.annotation.XmlElement;
import java.util.Set;

@WebService(serviceName = "WareController")
interface WareController {

    @WebMethod(operationName = "NewWare")
    @WebResult(name = "AddedWare")
    WareDto addWare(
        @WebParam(name = "Ware") @Valid @XmlElement(required = true) WareDto wareDto,
        @WebParam(name = "GroupId") @Valid @NotNull @XmlElement(required = true) @PositiveOrZero Long groupId
    );

    @WebMethod(operationName = "UpdateWare")
    @WebResult(name = "UpdatedWare")
    WareDto updateWare(@WebParam(name = "Ware") @Valid @XmlElement(required = true) WareDto wareDto);

    @WebMethod(operationName = "GetWare")
    @WebResult(name = "ware")
    WareDto getWareById(@WebParam(name = "WareId") @XmlElement(required = true) Long id);

    @WebMethod(operationName = "GetWareByName")
    @WebResult(name = "ware")
    WareDto getWareByName(@WebParam(name = "WareName") @XmlElement(required = true) String name);

    @WebMethod(operationName = "DeleteWare")
    void deleteById(@WebParam(name = "id") @XmlElement(required = true) @Positive Long id);

    @WebMethod(operationName = "GetWaresByStatus")
    @WebResult(name = "ware")
    Set<WareDto> getWaresByStatus(@WebParam(name = "status") @Valid @XmlElement(required = true) WareStatus status);

    @WebMethod(operationName = "GetAllWaresWithPages")
    @WebResult(name = "wares")
    WareDtoPage getWaresWithPages(
        @WebParam(name = "pageNo") @XmlElement(required = true) @PositiveOrZero Integer page,
        @WebParam(name = "itemCount") @XmlElement(required = true) @Positive Integer itemCount
    );

    @WebMethod(operationName = "MoveWareToGroup")
    @WebResult(name = "ware")
    void moveWareToGroup(
        @WebParam(name = "wareId") @XmlElement(required = true) Long wareId,
        @WebParam(name = "groupId") @XmlElement(required = true) Long groupId
    );

    @WebMethod(operationName = "GetAllWares")
    @WebResult(name = "ware")
    Set<WareDto> getWares();
}