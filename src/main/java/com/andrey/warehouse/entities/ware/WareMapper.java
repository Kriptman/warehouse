package com.andrey.warehouse.entities.ware;

import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper
public interface WareMapper {

    Ware toWare(WareDto wareDto);

    WareDto toWareDto(Ware ware);

    Set<WareDto> toWareDtoSet(Iterable<Ware> wareSet);

    List<WareDto> toWareDtoList(Iterable<Ware> wareSet);

    Set<Ware> toWareSet(Iterable<WareDto> wareDtoSet);
}
