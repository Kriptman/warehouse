package com.andrey.warehouse.entities.ware;

import com.andrey.warehouse.entities.group.Group;
import com.andrey.warehouse.entities.group.GroupService;
import com.andrey.warehouse.entities.wareDetail.WareDetail;
import com.andrey.warehouse.exceptions.InternalException;
import com.andrey.warehouse.exceptions.NotFoundException;
import com.andrey.warehouse.utils.LOG;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class WareServiceImpl implements WareService {

    private final WareRepository wareRepository;
    private final GroupService groupService;
    private final WareMapper mapper;

    @Override
    public Ware findById(Long id) {
        Optional<Ware> wareOptional = wareRepository.findById(id);
        if (!wareOptional.isPresent()) {
            NotFoundException.Throw("Ware", "id", id.toString());
        }
        return wareOptional.get();
    }

    @Override
    @Transactional
    public WareDto saveWareDto(WareDto wareDto, Long groupId) {
        Ware ware = mapper.toWare(wareDto);
        Group group = groupService.findByIdWithWares(groupId);
        group.addWare(ware);
        wareRepository.save(ware);
        LOG.logger().debug("Saved ware " + ware);
        return mapper.toWareDto(ware);
    }

    @Override
    public WareDto findWareDtoById(Long id) {
        LOG.logger().debug("Find ware with id = " + id);
        return mapper.toWareDto(findById(id));
    }

    @Override
    public void deleteById(Long id) {
        LOG.logger().debug("Delete ware with id = " + id);
        Ware ware = findById(id);
        ware.setStatus(WareStatus.DELETED);
        wareRepository.save(ware);
    }

    @Override
    public Set<WareDto> findWaresByStatus(WareStatus status) {
        LOG.logger().debug("Select wares by status " + status);
        return mapper.toWareDtoSet(wareRepository.findAllByStatus(status));
    }

    @Override
    @Transactional
    public void moveWareToGroup(Long wareId, Long groupId) {
        LOG.logger().debug("Move ware by id = {} to group id = {}", wareId, groupId);
        Ware ware = findById(wareId);
        Group group = groupService.findByIdWithWares(groupId);
        group.addWare(ware);
        wareRepository.save(ware);
    }

    @Override
    public Page<WareDto> findWaresWithPages(Pageable pageable) {
        LOG.logger().debug("Get wares with pages, " + pageable);
        Page<Ware> wares = wareRepository.findAll(pageable);
        List<WareDto> wareDtoList = mapper.toWareDtoList(wares);
        return new PageImpl<>(wareDtoList, pageable, wares.getTotalElements());
    }

    @Override
    public Set<WareDto> findWares() {
        LOG.logger().debug("Get wares");
        return mapper.toWareDtoSet(wareRepository.findAll());
    }

    @Override
    public WareDto updateWareDto(WareDto wareDto) {
        if (wareDto == null) return null;

        LOG.logger().debug("Update ware with id = " + wareDto.getId());
        Ware wareFromDb = findById(wareDto.getId());
        Ware ware = mapper.toWare(wareDto);
        if (ware == null) {
            throw new InternalException("Ware conversion not successful");
        }
        LOG.logger().debug("Ware before update " + wareFromDb);
        ware.setVersion(wareFromDb.getVersion());
        WareDetail detail = wareFromDb.getWareDetail();
        ware.getWareDetail().setVersion(detail.getVersion());
        ware.setGroup(wareFromDb.getGroup());
        ware.setWareProperties(wareFromDb.getWareProperties());

        wareRepository.save(ware);
        LOG.logger().debug("Ware after update " + ware);
        return mapper.toWareDto(ware);
    }

    @Override
    public Ware findByName(String name) {
        LOG.logger().debug("Find ware by name = " + name);
        Optional<Ware> wareOptional = wareRepository.findFirstByName(name);
        if (!wareOptional.isPresent()) {
            NotFoundException.Throw("Ware", "name", name);
        }
        return wareOptional.get();
    }

    @Override
    public WareDto findWareDtoByName(String name) {
        return mapper.toWareDto(findByName(name));
    }
}
