package com.andrey.warehouse.entities.ware;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

@Data
@XmlRootElement(name = "warePage")
@XmlAccessorType(XmlAccessType.FIELD)
@ToString
@EqualsAndHashCode
public class WareDtoPage {

    @XmlElement(name = "ware")
    private Set<WareDto> wares = new HashSet<>();
    private int totalPages;
    private Long totalItems;
}
