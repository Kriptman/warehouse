package com.andrey.warehouse.entities.ware;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface WareRepository extends JpaRepository<Ware, Long> {

    @Override
    @EntityGraph(value = Ware.DETAIL_AND_PROPERTIES_GRAPH, type = EntityGraph.EntityGraphType.FETCH)
    <S extends Ware> S save(S entity);

    @Override
    @EntityGraph(value = Ware.DETAIL_AND_PROPERTIES_GRAPH, type = EntityGraph.EntityGraphType.LOAD)
    Optional<Ware> findById(Long aLong);

    @Override
    @EntityGraph(value = Ware.DETAIL_AND_PROPERTIES_GRAPH, type = EntityGraph.EntityGraphType.LOAD)
    List<Ware> findAll();

    @EntityGraph(value = Ware.DETAIL_AND_PROPERTIES_GRAPH, type = EntityGraph.EntityGraphType.LOAD)
    Optional<Ware> findFirstByName(String name);

    @EntityGraph(value = Ware.DETAIL_AND_PROPERTIES_GRAPH, type = EntityGraph.EntityGraphType.LOAD)
    Set<Ware> findAllByStatus(WareStatus status);

    @Override
    @EntityGraph(value = Ware.DETAIL_AND_PROPERTIES_GRAPH, type = EntityGraph.EntityGraphType.LOAD)
    Page<Ware> findAll(Pageable pageable);
}
