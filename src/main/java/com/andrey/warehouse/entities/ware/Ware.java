package com.andrey.warehouse.entities.ware;

import com.andrey.warehouse.entities.group.Group;
import com.andrey.warehouse.entities.wareDetail.WareDetail;
import com.andrey.warehouse.entities.wareProperty.WareProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@ToString(exclude = {"wareDetail", "wareProperties", "group"})
@EqualsAndHashCode(exclude = {"wareDetail", "wareProperties", "group"})
@Audited
@NamedEntityGraphs({
    @NamedEntityGraph(
        name = Ware.DETAIL_GRAPH,
        attributeNodes = @NamedAttributeNode("wareDetail")
    ),
    @NamedEntityGraph(
        name = Ware.DETAIL_AND_PROPERTIES_GRAPH,
        attributeNodes = {
            @NamedAttributeNode("wareDetail"),
            @NamedAttributeNode("wareProperties"),
        }
    ),
    @NamedEntityGraph(
        name = Ware.ALL_PROPERTIES_GRAPH,
        attributeNodes = {
            @NamedAttributeNode("wareDetail"),
            @NamedAttributeNode("wareProperties"),
            @NamedAttributeNode("group")
        }
    )}
)
public class Ware {
    public static final String DETAIL_GRAPH = "Ware.Detail";
    public static final String DETAIL_AND_PROPERTIES_GRAPH = "Ware.DetailAndProperties";
    public static final String ALL_PROPERTIES_GRAPH = "Ware.AllProperties";

    @Id
    @OrderColumn
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Long version;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;
    private String name;
    private String shortName;
    private double nds;
    private Boolean useWareNds;
    @Enumerated(value = EnumType.STRING)
    private WareStatus status;

    @OneToOne(mappedBy = "ware", orphanRemoval = true, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    private WareDetail wareDetail;

    @OneToMany(mappedBy = "ware", orphanRemoval = true, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Set<WareProperty> wareProperties = new HashSet<>();

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @NotAudited
    private Group group;

    public void addProperty(WareProperty property) {
        wareProperties.add(property);
        property.setWare(this);
    }

    public void setWareDetail(WareDetail wareDetail) {
        this.wareDetail = wareDetail;
        wareDetail.setWare(this);
    }
}
