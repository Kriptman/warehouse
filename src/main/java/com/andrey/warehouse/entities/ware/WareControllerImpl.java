package com.andrey.warehouse.entities.ware;

import com.andrey.warehouse.exceptions.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.HashSet;
import java.util.Set;

@RequiredArgsConstructor
public class WareControllerImpl implements WareController {

    private final WareService wareService;

    @Override
    public WareDto addWare(WareDto wareDto, Long groupId) {
        wareDto.setId(null);
        wareDto.getWareDetail().setId(null);
        return wareService.saveWareDto(wareDto, groupId);
    }

    @Override
    public WareDto updateWare(WareDto wareDto) {
        if (wareDto.getId() == null) {
            throw new NotFoundException("Ware Id not found");
        }
        return wareService.updateWareDto(wareDto);
    }

    @Override
    public WareDto getWareById(Long id) {
        return wareService.findWareDtoById(id);
    }

    @Override
    public WareDto getWareByName(String name) {
        return wareService.findWareDtoByName(name);
    }

    @Override
    public void deleteById(Long id) {
        wareService.deleteById(id);
    }

    @Override
    public Set<WareDto> getWaresByStatus(WareStatus status) {
        return wareService.findWaresByStatus(status);
    }

    @Override
    public WareDtoPage getWaresWithPages(Integer page, Integer itemCount) {
        Page<WareDto> waresDtoPage = wareService.findWaresWithPages(PageRequest.of(page, itemCount));
        WareDtoPage pageDto = new WareDtoPage();
        pageDto.setWares(new HashSet<>(waresDtoPage.getContent()));
        pageDto.setTotalItems(waresDtoPage.getTotalElements());
        pageDto.setTotalPages(waresDtoPage.getTotalPages());
        return pageDto;
    }

    @Override
    public void moveWareToGroup(Long wareId, Long groupId) {
        wareService.moveWareToGroup(wareId, groupId);
    }

    @Override
    public Set<WareDto> getWares() {
        return wareService.findWares();
    }
}
