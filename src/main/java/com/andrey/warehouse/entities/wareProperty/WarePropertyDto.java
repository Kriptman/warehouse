package com.andrey.warehouse.entities.wareProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.*;

@Data
@ToString
@EqualsAndHashCode
@XmlRootElement(name = "wareProperty")
@XmlAccessorType(XmlAccessType.FIELD)
public class WarePropertyDto {

    @NotNull
    @XmlElement(required = true)
    private WarePropertyId id;

    @Size(max = 255)
    @NotNull
    @XmlElement(required = true)
    private String value;
}
