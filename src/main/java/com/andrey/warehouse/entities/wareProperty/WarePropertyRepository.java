package com.andrey.warehouse.entities.wareProperty;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface WarePropertyRepository extends CrudRepository<WareProperty, WarePropertyId> {

    Set<WareProperty> findAllByIdWareId(Long id);

    Set<WareProperty> findAllByIdPropertyId(Long id);

    @Override
    @EntityGraph(value = WareProperty.ALL_PROPERTIES_GRAPH, type = EntityGraph.EntityGraphType.FETCH)
    <S extends WareProperty> S save(S entity);
}
