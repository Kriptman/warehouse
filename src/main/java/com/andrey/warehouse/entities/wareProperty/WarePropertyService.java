package com.andrey.warehouse.entities.wareProperty;

import java.util.Set;

public interface WarePropertyService {

    WareProperty findById(WarePropertyId id);

    WarePropertyDto saveProperty(WarePropertyDto property);

    WarePropertyDto updatePropertyDto(WarePropertyDto property);

    Set<WarePropertyDto> findPropertiesByWareId(Long id);

    Set<WarePropertyDto> findPropertiesByPropertyId(Long id);

    Set<WarePropertyDto> findProperties();

    WarePropertyDto findPropertyDtoById(WarePropertyId id);
}
