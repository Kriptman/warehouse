package com.andrey.warehouse.entities.wareProperty;

import com.andrey.warehouse.entities.property.Property;
import com.andrey.warehouse.entities.ware.Ware;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@EqualsAndHashCode(exclude = {"ware", "property"})
@ToString(exclude = {"ware", "property"})
@Audited
@NamedEntityGraph(
    name = WareProperty.ALL_PROPERTIES_GRAPH,
    attributeNodes = {@NamedAttributeNode("property"), @NamedAttributeNode("ware")}
)
public class WareProperty {
    public static final String ALL_PROPERTIES_GRAPH = "WareProperty.WareAndProperty";

    @EmbeddedId
    @OrderColumn
    private WarePropertyId id;

    @Version
    private Long version;

    @UpdateTimestamp
    private LocalDateTime updateTimestamp;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @MapsId("wareId")
    private Ware ware;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @MapsId("propertyId")
    private Property property;

    private String value;
}
