package com.andrey.warehouse.entities.wareProperty;

import com.andrey.warehouse.entities.property.Property;
import com.andrey.warehouse.entities.property.PropertyService;
import com.andrey.warehouse.entities.ware.Ware;
import com.andrey.warehouse.entities.ware.WareService;
import com.andrey.warehouse.exceptions.InternalException;
import com.andrey.warehouse.exceptions.NotFoundException;
import com.andrey.warehouse.exceptions.WrongValueException;
import com.andrey.warehouse.utils.LOG;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class WarePropertyServiceImpl implements WarePropertyService {

    private final WarePropertyRepository repository;
    private final WareService wareService;
    private final PropertyService propertyService;
    private final WarePropertyMapper mapper;

    @Override
    @Transactional
    public WarePropertyDto saveProperty(WarePropertyDto property) {
        Long wareId = property.getId().getWareId();
        Ware ware = wareService.findById(wareId);

        Long propId = property.getId().getPropertyId();
        Property prop = propertyService.findById(propId);

        WareProperty wareProperty = mapper.toWareProperty(property);
        if (wareProperty == null) {
            throw new InternalException("WareProperty conversion not successful");
        }

        if (!prop.getType().checkValue(wareProperty.getValue())) {
            throw new WrongValueException(
                String.format("Wrong value '%s' for property '%s' with type '%s'",
                    wareProperty.getValue(), prop.getName(), prop.getType()));
        }

        ware.addProperty(wareProperty);
        prop.addProperty(wareProperty);
        repository.save(wareProperty);

        LOG.logger().debug("Saved wareProperty " + wareProperty.toString());
        return mapper.toWarePropertyDto(wareProperty);
    }

    @Override
    public WareProperty findById(WarePropertyId id) {
        Optional<WareProperty> warePropertyOptional = repository.findById(id);
        if (!warePropertyOptional.isPresent()) {
            NotFoundException.Throw("WareProperty", "id", id.toString());
        }
        return warePropertyOptional.get();
    }

    @Override
    public WarePropertyDto updatePropertyDto(WarePropertyDto property) {
        if (property == null) return null;

        LOG.logger().debug("Update wareProperty with id = " + property.getId());
        WareProperty propFromDb = findById(property.getId());
        WareProperty wareProperty = mapper.toWareProperty(property);

        if (wareProperty == null) {
            throw new InternalException("WareProperty conversion not successful");
        }
        LOG.logger().debug("WareProperty before update " + propFromDb);
        wareProperty.setVersion(propFromDb.getVersion());

        repository.save(wareProperty);
        LOG.logger().debug("WareProperty after update " + wareProperty);
        return mapper.toWarePropertyDto(wareProperty);
    }

    @Override
    public Set<WarePropertyDto> findPropertiesByWareId(Long id) {
        LOG.logger().debug("Get properties by ware id " + id);
        return mapper.toWarePropertyDtoSet(repository.findAllByIdWareId(id));
    }

    @Override
    public Set<WarePropertyDto> findPropertiesByPropertyId(Long id) {
        LOG.logger().debug("Get properties by property id " + id);
        return mapper.toWarePropertyDtoSet(repository.findAllByIdPropertyId(id));
    }

    @Override
    public WarePropertyDto findPropertyDtoById(WarePropertyId id) {
        return mapper.toWarePropertyDto(findById(id));
    }

    @Override
    public Set<WarePropertyDto> findProperties() {
        LOG.logger().debug("Get all properties");
        return mapper.toWarePropertyDtoSet(repository.findAll());
    }
}
