package com.andrey.warehouse.entities.wareProperty;

import org.mapstruct.Mapper;

import java.util.Set;

@Mapper
public interface WarePropertyMapper {

    WareProperty toWareProperty(WarePropertyDto warePropertyDto);

    WarePropertyDto toWarePropertyDto(WareProperty wareProperty);

    Set<WarePropertyDto> toWarePropertyDtoSet(Iterable<WareProperty> warePropertySet);

    Set<WareProperty> toWarePropertySet(Iterable<WarePropertyDto> warePropertyDtoSet);
}
