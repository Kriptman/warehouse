package com.andrey.warehouse.entities.wareProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class WarePropertyId implements Serializable {

    @Column(name = "ware_id")
    @NotNull
    private Long wareId;
    @Column(name = "property_id")
    @NotNull
    private Long propertyId;
}
