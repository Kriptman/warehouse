package com.andrey.warehouse.entities.wareProperty;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.validation.Valid;
import javax.xml.bind.annotation.XmlElement;
import java.util.Set;

@WebService(serviceName = "warePropertyController")
interface WarePropertyController {

    @WebMethod(operationName = "NewWareProperty")
    @WebResult(name = "addedProperty")
    WarePropertyDto addProperty(@WebParam(name = "property") @Valid @XmlElement(required = true) WarePropertyDto property);

    @WebMethod(operationName = "UpdateWareProperty")
    @WebResult(name = "updatedProperty")
    WarePropertyDto updateProperty(@WebParam(name = "property") @Valid @XmlElement(required = true) WarePropertyDto property);

    @WebMethod(operationName = "GetWareProperty")
    @WebResult(name = "property")
    WarePropertyDto getPropertyById(@WebParam(name = "id") @Valid @XmlElement(required = true) WarePropertyId id);

    @WebMethod(operationName = "GetWarePropertyByWareId")
    @WebResult(name = "property")
    Set<WarePropertyDto> getPropertiesByWareId(@WebParam(name = "id") @XmlElement(required = true) Long id);

    @WebMethod(operationName = "GetWarePropertyByPropertyId")
    @WebResult(name = "property")
    Set<WarePropertyDto> getPropertiesByPropertyId(@WebParam(name = "id") @XmlElement(required = true) Long id);

    @WebMethod(operationName = "GetAllWareProperties")
    @WebResult(name = "property")
    Set<WarePropertyDto> getProperties();
}
