package com.andrey.warehouse.entities.wareProperty;

import lombok.RequiredArgsConstructor;

import java.util.Set;

@RequiredArgsConstructor
public class WarePropertyControllerImpl implements WarePropertyController {

    private final WarePropertyService service;

    @Override
    public WarePropertyDto getPropertyById(WarePropertyId id) {
        return service.findPropertyDtoById(id);
    }

    @Override
    public WarePropertyDto addProperty(WarePropertyDto property) {
        return service.saveProperty(property);
    }

    @Override
    public WarePropertyDto updateProperty(WarePropertyDto property) {
        return service.updatePropertyDto(property);
    }

    @Override
    public Set<WarePropertyDto> getPropertiesByWareId(Long id) {
        return service.findPropertiesByWareId(id);
    }

    @Override
    public Set<WarePropertyDto> getPropertiesByPropertyId(Long id) {
        return service.findPropertiesByPropertyId(id);
    }

    @Override
    public Set<WarePropertyDto> getProperties() {
        return service.findProperties();
    }
}
