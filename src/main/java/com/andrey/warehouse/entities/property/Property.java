package com.andrey.warehouse.entities.property;

import com.andrey.warehouse.entities.wareProperty.WareProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(exclude = "wareProperties")
@ToString(exclude = "wareProperties")
@Audited
@NamedEntityGraph(
    name = Property.PROPERTIES_GRAPH,
    attributeNodes = @NamedAttributeNode("wareProperties")
)
public class Property {
    public static final String PROPERTIES_GRAPH = "Property.Properties";

    @Id
    @OrderColumn
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Long version;

    @UpdateTimestamp
    private LocalDateTime updateTimestamp;

    private String name;

    @Enumerated(value = EnumType.STRING)
    private PropertyType type;

    @OneToMany(mappedBy = "property", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
    private Set<WareProperty> wareProperties = new HashSet<>();

    public void addProperty(WareProperty property) {
        wareProperties.add(property);
        property.setProperty(this);
    }
}
