package com.andrey.warehouse.entities.property;

import org.mapstruct.Mapper;

import java.util.Set;

@Mapper
public interface PropertyMapper {

    Property toProperty(PropertyDto propertyDto);

    PropertyDto toPropertyDto(Property property);

    Set<PropertyDto> toPropertyDtoSet(Iterable<Property> propertySet);

    Set<Property> toPropertySet(Iterable<PropertyDto> propertyDtoSet);
}
