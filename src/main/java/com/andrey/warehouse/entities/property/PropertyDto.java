package com.andrey.warehouse.entities.property;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@ToString
@EqualsAndHashCode
@XmlRootElement(name = "property")
@XmlAccessorType(XmlAccessType.FIELD)
public class PropertyDto {

    @PositiveOrZero
    private Long id;

    @Size(max = 255)
    @NotNull
    @XmlElement(required = true)
    private String name;

    @XmlElement(required = true)
    @NotNull
    private PropertyType type;
}
