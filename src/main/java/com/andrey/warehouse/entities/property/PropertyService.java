package com.andrey.warehouse.entities.property;

import java.util.Set;

public interface PropertyService {

    Property findById(Long id);

    PropertyDto saveProperty(PropertyDto propertyDto);

    PropertyDto updateProperty(PropertyDto propertyDto);

    PropertyDto findPropertyDtoById(Long id);

    Set<PropertyDto> findProperties();

    PropertyDto findPropertyDtoByName(String name);

    Property findPropertyByName(String name);
}
