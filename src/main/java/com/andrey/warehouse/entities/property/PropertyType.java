package com.andrey.warehouse.entities.property;

import com.andrey.warehouse.utils.LOG;
import org.apache.commons.lang3.math.NumberUtils;

import java.text.NumberFormat;
import java.text.ParseException;

public enum PropertyType {
    STRING,
    CURRENCY,
    INTEGER,
    DOUBLE,
    BOOLEAN;

    public boolean checkValue(String value) {
        switch (this) {
            case STRING:
                return true;
            case CURRENCY:
                try {
                    NumberFormat.getCurrencyInstance().parse(value).doubleValue();
                    return true;
                } catch (ParseException e) {
                    LOG.logger().debug(e.getMessage());
                    return false;
                }
            case INTEGER:
                return NumberUtils.isDigits(value);
            case DOUBLE:
                return NumberUtils.isParsable(value);
            case BOOLEAN:
                return value.toUpperCase().equals("TRUE") || value.toUpperCase().equals("FALSE");
        }
        return false;
    }
}
