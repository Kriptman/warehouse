package com.andrey.warehouse.entities.property;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PropertyRepository extends CrudRepository<Property, Long> {
    Optional<Property> findFirstByName(String name);

    @Override
    @EntityGraph(value = Property.PROPERTIES_GRAPH, type = EntityGraph.EntityGraphType.FETCH)
    <S extends Property> S save(S entity);

    @Override
    @EntityGraph(value = Property.PROPERTIES_GRAPH, type = EntityGraph.EntityGraphType.LOAD)
    Optional<Property> findById(Long aLong);
}
