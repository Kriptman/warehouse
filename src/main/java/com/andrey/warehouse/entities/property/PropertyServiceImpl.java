package com.andrey.warehouse.entities.property;

import com.andrey.warehouse.exceptions.InternalException;
import com.andrey.warehouse.exceptions.NotFoundException;
import com.andrey.warehouse.utils.LOG;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class PropertyServiceImpl implements PropertyService {

    private final PropertyRepository repository;
    private final PropertyMapper mapper;


    @Override
    public PropertyDto findPropertyDtoById(Long id) {
        LOG.logger().debug("Find property with id " + id);
        return mapper.toPropertyDto(findById(id));
    }

    @Override
    public Property findPropertyByName(String name) {
        LOG.logger().debug("Find property by name " + name);
        Optional<Property> propertyOptional = repository.findFirstByName(name);
        if (!propertyOptional.isPresent()) {
            NotFoundException.Throw("Property", "name", name);
        }
        return propertyOptional.get();
    }

    @Override
    public PropertyDto findPropertyDtoByName(String name) {
        return mapper.toPropertyDto(findPropertyByName(name));
    }

    @Override
    public Set<PropertyDto> findProperties() {
        return mapper.toPropertyDtoSet(repository.findAll());
    }

    @Override
    public Property findById(Long id) {
        Optional<Property> propertyOptional = repository.findById(id);
        if (!propertyOptional.isPresent()) {
            NotFoundException.Throw("Property", "name", id.toString());
        }
        return propertyOptional.get();
    }

    @Override
    public PropertyDto saveProperty(PropertyDto propertyDto) {
        Property property = mapper.toProperty(propertyDto);
        repository.save(property);
        LOG.logger().debug("Saved property " + property);
        return mapper.toPropertyDto(property);
    }

    @Override
    public PropertyDto updateProperty(PropertyDto propertyDto) {
        if (propertyDto == null) return null;

        LOG.logger().debug("Update property with id " + propertyDto.getId());
        Property propertyFromDb = findById(propertyDto.getId());
        Property propertyToSave = mapper.toProperty(propertyDto);
        if (propertyToSave == null) {
            throw new InternalException("Property conversion not successful");
        }
        LOG.logger().debug("Property before update " + propertyFromDb);
        propertyToSave.setVersion(propertyFromDb.getVersion());

        repository.save(propertyToSave);
        LOG.logger().debug("Property after update " + propertyToSave);
        return mapper.toPropertyDto(propertyToSave);
    }
}
