package com.andrey.warehouse.entities.property;

import com.andrey.warehouse.exceptions.NotFoundException;
import com.andrey.warehouse.utils.LOG;
import lombok.RequiredArgsConstructor;

import java.util.Set;

@RequiredArgsConstructor
public class PropertyControllerImpl implements PropertyController {
    private final PropertyService service;

    @Override
    public PropertyDto addProperty(PropertyDto propertyDto) {
        propertyDto.setId(null);
        LOG.logger().debug("Add property " + propertyDto);
        return service.saveProperty(propertyDto);
    }

    @Override
    public PropertyDto updateProperty(PropertyDto propertyDto) {
        LOG.logger().debug("Update property " + propertyDto);
        if (propertyDto.getId() == null) {
            throw new NotFoundException("Property ID not found");
        }
        return service.updateProperty(propertyDto);
    }

    @Override
    public PropertyDto getPropertyById(Long id) {
        LOG.logger().debug("Select property " + id);
        return service.findPropertyDtoById(id);
    }

    @Override
    public PropertyDto getPropertyByName(String name) {
        LOG.logger().debug("Select property by name " + name);
        return service.findPropertyDtoByName(name);
    }

    @Override
    public Set<PropertyDto> getProperties() {
        LOG.logger().debug("Select properties");
        return service.findProperties();
    }
}
