package com.andrey.warehouse.entities.property;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import java.util.Set;

@WebService(name = "PropertyController")
interface PropertyController {

    @WebMethod(operationName = "NewProperty")
    @WebResult(name = "AddedProperty")
    PropertyDto addProperty(@WebParam(name = "property") @Valid @XmlElement(required = true) PropertyDto propertyDto);

    @WebMethod(operationName = "UpdateProperty")
    @WebResult(name = "UpdatedProperty")
    PropertyDto updateProperty(@WebParam(name = "property") @Valid @XmlElement(required = true) PropertyDto propertyDto);

    @WebMethod(operationName = "GetProperty")
    @WebResult(name = "Property")
    PropertyDto getPropertyById(@WebParam(name = "id") @XmlElement(required = true) Long id);

    @WebMethod(operationName = "GetPropertyByName")
    @WebResult(name = "Property")
    PropertyDto getPropertyByName(@WebParam(name = "name") @XmlElement(required = true) @NotNull String name);

    @WebMethod(operationName = "GetAllProperties")
    @WebResult(name = "Property")
    Set<PropertyDto> getProperties();
}
