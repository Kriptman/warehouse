package com.andrey.warehouse.entities.wareDetail;

import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.PositiveOrZero;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "wareDetail")
@XmlAccessorType(XmlAccessType.FIELD)
public class WareDetailDto {

    private Long id;

    @PositiveOrZero
    private int quantity;

    @PositiveOrZero
    @Digits(integer = 10, fraction = 2)
    private double price;
}
