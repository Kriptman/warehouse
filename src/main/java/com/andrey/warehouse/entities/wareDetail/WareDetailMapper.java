package com.andrey.warehouse.entities.wareDetail;

import org.mapstruct.Mapper;

@Mapper
public interface WareDetailMapper {

    WareDetail toWareDetail(WareDetailDto wareDetailDto);

    WareDetailDto toWareDetailDto(WareDetail wareDetail);
}
