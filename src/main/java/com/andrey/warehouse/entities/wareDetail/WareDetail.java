package com.andrey.warehouse.entities.wareDetail;

import com.andrey.warehouse.entities.ware.Ware;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@ToString(exclude = "ware")
@EqualsAndHashCode(exclude = "ware")
@Audited
public class WareDetail {

    @Id
    @OrderColumn
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Version
    private Long version;
    @UpdateTimestamp
    private LocalDateTime updateDateTime;
    private int quantity;
    private double price;

    @OneToOne(fetch = FetchType.LAZY)
    private Ware ware;
}
