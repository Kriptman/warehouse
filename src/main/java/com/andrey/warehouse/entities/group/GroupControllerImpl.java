package com.andrey.warehouse.entities.group;

import com.andrey.warehouse.exceptions.NotFoundException;
import com.andrey.warehouse.utils.LOG;

public class GroupControllerImpl implements GroupController {

    private final GroupService service;

    public GroupControllerImpl(GroupService service) {
        this.service = service;
    }

    @Override
    public GroupDto addGroup(GroupDto groupDto, Long parentId) {
        LOG.logger().debug("Add group {} to parent {}", groupDto, parentId);
        throwIfNull(parentId);
        groupDto.setId(null);
        return service.saveGroup(groupDto, parentId);
    }

    @Override
    public GroupDto updateGroup(GroupDto groupDto) {
        LOG.logger().debug("Update group " + groupDto);
        if (groupDto.getId() == null) {
            throw new NotFoundException("Group ID not found");
        }
        return service.updateGroup(groupDto);
    }

    @Override
    public GroupDto getGroupById(Long id) {
        LOG.logger().debug("Select group by id " + id);
        return service.findGroupDtoById(id);
    }

    @Override
    public GroupDto getGroupByName(String name) {
        LOG.logger().debug("Select group by name " + name);
        return service.findGroupDtoByName(name);
    }

    @Override
    public void moveToGroup(Long groupId, Long newParentId) {
        LOG.logger().debug("Move group {} to new parent {}", groupId, newParentId);
        throwIfNull(newParentId);
        service.moveToGroup(groupId, newParentId);
    }

    private void throwIfNull(Long newParentId) {
        if (newParentId == null) {
            throw new IllegalArgumentException("Parent required");
        }
    }

    @Override
    public GroupDto getGroups() {
        LOG.logger().debug("Select all groups");
        return service.findGroups();
    }
}
