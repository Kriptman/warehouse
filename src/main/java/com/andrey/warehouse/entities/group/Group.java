package com.andrey.warehouse.entities.group;

import com.andrey.warehouse.entities.ware.Ware;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity(name = Group.ENTITY_NAME)
@Data
@EqualsAndHashCode(exclude = {"parent", "wares", "childes"})
@ToString(exclude = {"parent", "wares", "childes"})
@NoArgsConstructor
@NamedEntityGraph(
    name = Group.PROPERTIES_GRAPH,
    attributeNodes = {
        @NamedAttributeNode("wares"),
        @NamedAttributeNode("childes"),
        @NamedAttributeNode("parent")
    }
)
@NamedQueries(value = {
    @NamedQuery(
        name = Group.QUERY_ALL_FIELDS_BY_ID,
        query = "SELECT g FROM ware_group g LEFT JOIN FETCH g.wares w LEFT JOIN FETCH w.wareDetail LEFT JOIN FETCH w.wareProperties LEFT JOIN FETCH g.childes"
    ),
    @NamedQuery(
        name = Group.QUERY_ALL_FIELDS_BY_NAME,
        query = "SELECT g FROM ware_group g LEFT JOIN FETCH g.wares w LEFT JOIN FETCH w.wareDetail LEFT JOIN FETCH w.wareProperties LEFT JOIN FETCH g.childes"
    )
})
public class Group {
    public static final String QUERY_ALL_FIELDS_BY_ID = "Group.QUERY_ALL_FIELDS_BY_ID";
    public static final String QUERY_ALL_FIELDS_BY_NAME = "Group.QUERY_ALL_FIELDS_BY_NAME";
    public static final String PROPERTIES_GRAPH = "Group.Properties";
    public static final String ENTITY_NAME = "ware_group";
    public static final Long ROOT_ID = 0L;

    @Id
    @OrderColumn
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Long version;

    @UpdateTimestamp
    private LocalDateTime updateTimestamp;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private Group parent;

    @OneToMany(mappedBy = "parent", orphanRemoval = true, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Set<Group> childes = new HashSet<>();

    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY)
    private Set<Ware> wares = new HashSet<>();

    public Group(String name, Group parent) {
        if (parent == null) {
            throw new IllegalArgumentException("Parent required");
        }
        this.name = name;
        this.parent = parent;
        regInChildes();
    }

    private void regInChildes() {
        parent.childes.add(this);
    }

    public void addChild(Group group) {
        childes.add(group);
        group.setParent(this);
    }

    public void addWare(Ware ware) {
        wares.add(ware);
        ware.setGroup(this);
    }
}
