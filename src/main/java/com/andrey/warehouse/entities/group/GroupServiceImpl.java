package com.andrey.warehouse.entities.group;

import com.andrey.warehouse.exceptions.InternalException;
import com.andrey.warehouse.exceptions.NotFoundException;
import com.andrey.warehouse.utils.LOG;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {

    private final GroupRepository repository;
    private final GroupMapper mapper;
    private final EntityManager entityManager;

    @Override
    @Transactional
    public GroupDto findGroupDtoById(Long id) {
        LOG.logger().debug("Find group with id " + id);
        return mapper.toGroupDto(findByIdWithWares(id));
    }

    @Override
    @Transactional
    public void moveToGroup(Long groupId, Long newParentId) {
        LOG.logger().debug("Move group by id {} to new parent {}", groupId, newParentId);
        Group group = findByIdWithWares(groupId);
        Group newParent = findByIdWithWares(newParentId);
        newParent.addChild(group);
        repository.save(group);
    }

    @Override
    public Group findGroupByName(String name) {
        LOG.logger().debug("Find group by name " + name);
        Optional<Group> groupOptional = repository.findFirstByName(name);
        if (!groupOptional.isPresent()) {
            NotFoundException.Throw("Group", "name", name);
        }
        return groupOptional.get();
    }

    @Override
    public Group findByIdWithWares(Long id) {
        LOG.logger().debug("Find group by id " + id);
        entityManager.createNamedQuery(Group.QUERY_ALL_FIELDS_BY_ID).getResultList();
        Group group = entityManager.find(Group.class, id);
        if (group == null) {
            NotFoundException.Throw("Group", "id", id.toString());
        }
        return group;
    }

    @Override
    @Transactional
    public GroupDto findGroupDtoByName(String name) {
        return mapper.toGroupDto(findGroupByName(name));
    }

    @Override
    @Transactional
    public GroupDto findGroups() {
        Group group = findByIdWithWares(Group.ROOT_ID);
        return mapper.toGroupDto(group);
    }

    @Override
    @Transactional
    public GroupDto saveGroup(GroupDto groupDto, Long parentId) {
        Group group = mapper.toGroup(groupDto);
        if (group == null) {
            throw new InternalException("Group conversion not successful");
        }
        group.setParent(findByIdWithWares(parentId));
        repository.save(group);
        LOG.logger().debug("Saved group " + group);
        return mapper.toGroupDto(group);
    }

    @Override
    @Transactional
    public GroupDto updateGroup(GroupDto groupDto) {
        if (groupDto == null) return null;

        LOG.logger().debug("Update group with id = " + groupDto.getId());
        Group group = findByIdWithWares(groupDto.getId());
        group.setName(groupDto.getName());

        repository.save(group);
        LOG.logger().debug("Group after update " + group);
        return mapper.toGroupDto(group);
    }
}
