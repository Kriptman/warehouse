package com.andrey.warehouse.entities.group;

import org.mapstruct.Mapper;

import java.util.Set;

@Mapper
public interface GroupMapper {

    Group toGroup(GroupDto groupDto);

    GroupDto toGroupDto(Group group);

    Set<GroupDto> toGroupDtoSet(Set<Group> groupSet);

    Set<Group> toGroupSet(Set<GroupDto> groupDtoSet);
}
