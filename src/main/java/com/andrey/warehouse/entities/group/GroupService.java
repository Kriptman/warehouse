package com.andrey.warehouse.entities.group;

public interface GroupService {

    Group findByIdWithWares(Long id);

    GroupDto saveGroup(GroupDto groupDto, Long parentId);

    GroupDto updateGroup(GroupDto groupDto);

    GroupDto findGroupDtoById(Long id);

    GroupDto findGroups();

    GroupDto findGroupDtoByName(String name);

    Group findGroupByName(String name);

    void moveToGroup(Long groupId, Long newParentId);
}
