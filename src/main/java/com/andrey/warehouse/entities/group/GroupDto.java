package com.andrey.warehouse.entities.group;

import com.andrey.warehouse.entities.ware.WareDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

@Data
@ToString
@EqualsAndHashCode
@XmlRootElement(name = "property")
@XmlAccessorType(XmlAccessType.FIELD)
public class GroupDto {

    @PositiveOrZero
    private Long id;

    @Size(max = 255)
    @NotNull
    @XmlElement(required = true)
    private String name;

    private Set<WareDto> wares = new HashSet<>();
    private Set<GroupDto> childes = new HashSet<>();
}
