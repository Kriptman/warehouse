package com.andrey.warehouse.entities.group;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface GroupRepository extends CrudRepository<Group, Long> {

    @EntityGraph(value = Group.PROPERTIES_GRAPH, type = EntityGraph.EntityGraphType.LOAD)
    Optional<Group> findFirstByName(String name);

    @Override
    @EntityGraph(value = Group.PROPERTIES_GRAPH, type = EntityGraph.EntityGraphType.FETCH)
    <S extends Group> S save(S entity);
}
