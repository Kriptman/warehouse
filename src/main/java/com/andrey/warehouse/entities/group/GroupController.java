package com.andrey.warehouse.entities.group;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.validation.Valid;
import javax.validation.constraints.PositiveOrZero;
import javax.xml.bind.annotation.XmlElement;

@WebService(name = "GroupController")
interface GroupController {

    @WebMethod(operationName = "NewGroup")
    @WebResult(name = "AddedGroup")
    GroupDto addGroup(
        @WebParam(name = "group") @Valid @XmlElement(required = true) GroupDto groupDto,
        @WebParam(name = "parentId") @XmlElement(required = true) @PositiveOrZero Long parentId
    );

    @WebMethod(operationName = "UpdateGroup")
    @WebResult(name = "UpdatedGroup")
    GroupDto updateGroup(@WebParam(name = "group") @Valid @XmlElement(required = true) GroupDto groupDto);

    @WebMethod(operationName = "GetGroup")
    @WebResult(name = "Group")
    GroupDto getGroupById(@WebParam(name = "id") @XmlElement(required = true) Long id);

    @WebMethod(operationName = "GetGroupByName")
    @WebResult(name = "Group")
    GroupDto getGroupByName(@WebParam(name = "name") @XmlElement(required = true) String name);

    @WebMethod(operationName = "GetAllGroups")
    @WebResult(name = "Group")
    GroupDto getGroups();

    @WebMethod(operationName = "MoveToGroup")
    @WebResult(name = "Group")
    void moveToGroup(
        @WebParam(name = "groupId") @XmlElement(required = true) Long groupId,
        @WebParam(name = "new_parent_id") @XmlElement(required = true) @PositiveOrZero Long newParentId
    );
}
